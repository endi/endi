/**
 * Vue app for sale file management
 */
import { startApp } from '@/helpers/utils'
import App from './App.vue'

const app = startApp(App, 'vue-file-app-container')
