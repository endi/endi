Export comptables des documents et paiements
=============================================

Les vues sont dans caerp/views/export/

Les outils de production d'écritures sont dans caerp/compute/

Les outils d'export vers des fichiers sont dans caerp/export/

Les interfaces utilisées pour plugger des modules d'exports sont dans caerp/interfaces.py


Important :

Les formats de données et le format des fichiers de sortis sont dépendant des ExportProducer et ExportWriter qui sont configurés dans le fichier .ini.

Par exemple les écritures produites par défaut contiennent une ligne générale et une ligne analytique, ce n'est pas forcément le cas.

De même les fichiers de sorties ne sont pas forcément en csv et ne peuvent aussi des entêtes différentes de celles par défaut.

.. toctree::
  :maxdepth: 2

  sage.rst
  sage_generation_expert.rst
  cegid.rst
  isacompta.rst
  export_comptable_module.rst
