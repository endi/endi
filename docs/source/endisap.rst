Passer caerp en mode SAP (Services à la personne)
================================================

Une instance peut être dédiée aux services à la personne (CAE SAP).

.. warning:: L'ensemble de l'instance enDi fonctionne alors en mode services à
   la personne : toutes les enseignes, toutes les factures… etc.


Dans le fichier .ini de l'application rajouter :

.. code-block:: console

   caerp.includes =
       caerp.plugins.sap

    [celery]
    imports =
        caerp.plugins.sap.celery_jobs


Dans le fichier .ini de caerp_celery rajouter :

.. code-block:: console


    pyramid.includes =
        caerp.plugins.sap.panels

    [celery]
    imports =
        caerp.plugins.sap.celery_jobs


cf aussi :doc:`decoupage`.
