Formulaire vuejs
=================

Nous utilisons la librairie vee-validate pour la validation des formulaires VueJs.

Exemple basique

.. code-block:: javascript

    <script setup>
    import { useForm } from 'vee-validate'
    import {
        getSubmitErrorCallback,
        getSubmitModelCallback,
    } from '@/helpers/form'
    import Input from '@/components/forms/Input.vue'
    import Button from '@/components/Button.vue'

    const emit = defineEmits(['saved', 'cancel', 'error'])

    const { handleSubmit } = useForm({
        initialValues: { test: 'valeur par défaut' },
    })
    const sendDataToServer = (values) => {
        console.log(values)
        return new Promise((resolve, reject) => values)
    }
    const onSubmitSuccess = getSubmitModelCallback(emit, sendDataToServer)
    const onSubmitError = getSubmitErrorCallback(emit)
    const onSubmit = handleSubmit(onSubmitSuccess, onSubmitError)
    </script>
    <template>
    <form @submit="onSubmit">
        <Input name="test" label="Test" />
        <Button type="submit" label="Valider" />
    </form>
    </template>