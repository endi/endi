Formulaires JS
===============

Plusieurs formulaires fonctionnent comme une One page Application.
Les pages d'éditions des documents, la configuration des statistiques, du
catalogue produit ...

Ces pages fonctionnent de manières similaires.

Cycle de chargement
--------------------

1. Chargement initial de la page par une requête http classique.
   Des urls sont passés à l'application Javascript via un objet JS global
   "AppOption"

2. Chargement des éléments de configuration via un appel à une api rest
   On charge les "options" disponibles au sein du formulaire et les "sections"
   qui décrivent les différents sections et champs du formulaire.

3. Chargement des différents modèles de données via des appels à une api rest


Configuration du formulaire
----------------------------

La configuration du formulaire est chargée côté javascript par
`src/base/components/ConfigBus.js`. Elle est composée de deux entrées:

    - Les options
    - Les sections


Options
........

Les options listent les valeurs sélectionnables par l'utilisateur (différents
taux de TVA, Compte produits ...)

Pour récupérer les options depuis une vue dans le code Js

.. code-block:: javascript

    import Radio from 'backbone-radio';

    ...
    const config = Radio.channel('config')
    const tva_options = config.request('get:options', 'tvas')


NB : les données renvoyées sont des "clones" de la version stockées dans le
channel de configuration. Ils peuvent être modifiés localement

Sections
.........

Les sections décrivent les différentes parties du formulaire.
La structure de données permet de définir les options à présenter à
l'utilisateur final, leur format dépend de celui attendu par le code JS, il n'y
a pas aujourd'hui de structure générique, même si un format se dessine
progressivement.

Il est possible de savoir si une définition est présente dans la config

Si la section ressemble à

.. code-block:: javascript

    {
        'composition': {
            'edit': true,
            'mode': 'ttc',
            'lines': {
                'can_add': true,
                'can_delete': true,
                'tva': {'edit': true}
            }
        }
    }



.. code-block:: javascript

    import Radio from 'backbone-radio';

    ...
    const config = Radio.channel('config')
    if (config.request('has:section', 'composition:lines:tva')){
       // On affiche le sélecteur de TVA dans un formulaire)


Il est possible de récupérer la définition


.. code-block:: javascript

    import Radio from 'backbone-radio';

    ...
    const config = Radio.channel('config')
    const section = config.request('get:section', 'composition:lines');
    if (section['can_add']){
        // On affiche un bouton "ajouter"
    }


 NB : les appels à `has:section` et `get:section` supporte une notation
 arborescente avec le séparateur ":"
