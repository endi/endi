Polymorphisme
==================

Le polymorphisme permet de fournir une structure d'héritage à nos modèles.

Duplication
-------------

Le polymorphisme permet de factoriser les déclarations d'attributs. La classe Node (et la table sql correspondante) stocke par exemple la date de création.

Requêtes
-------------

Grâce au polymorphisme, on peut effectuer des requêtes sur plusieurs modèles simultanément

.. code-block:: python

    Node.query().filter(Node.name=="Nom que l'on cherche")

Cette requête va permettre de requêter (et donc de trier/filtrer) tous les noeuds enfants de Node.
Pour filtrer par type il faut utiliser un filtre sur la clé de polymorphisme


.. code-block:: python

    Node.query().filter(Node.type_.in_(['invoice', 'cancelinvoice',
    'internalinvoice']))

Pour requêrir les attributs spécifiques aux modèles enfants en une seule requête
il est possible d'utiliser la méthode with_polymorphic


.. code-block:: python

    Node.query().with_polymorphic([Invoice, CancelInvoice])

NB : cela ne filtre pas sur le type de Node !!

Relations
-------------

Grâce à cette technique d'héritage, on peut configurer des relations moins typées.

Par exemple, les fichiers peuvent être lié à des dossiers, des devis, des factures et des avoirs, mais ce sont des modèles différents.
Compte tenu qu'ils héritent tous de Node, on a configuré une relation générique de Node à Node, qui nous permet de lier les Nodes entre eux, indifféremment de leur type.
On peut donc lier un fichier à un dossier, mais on pourrait, grâce à cette même relation, attacher un fichier à une activité.


Implémentation
-----------------

Aujourd'hui, plusieurs arbres polymorphiques sont implémentés dans CAERP, celui-ci est le prinicipal :

                                                       Node

                                  /                     |                 \
                                 /                      |                  \
                                /                       |                   \
                             Task                       File                Event
                         /    |     \                                           \
                Estimation  Invoice  CancelInvoice                              Activity

Autres implémentations:

* Les types de frais (BaseExpenseType, ExpenseType, ExpenseTelType, ExpenseKmType)
* Les frais (BaseExpenseLine, ExpenseLine, ExpenseTelLine, ExpenseKmLine)


