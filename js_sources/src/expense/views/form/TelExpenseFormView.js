import Radio from "backbone.radio"
import BaseExpenseFormView from "./BaseExpenseFormView.js"

const TelExpenseFormView = BaseExpenseFormView.extend({
    childViewEvents: {
        finish: "onChildChange",
        change: "onAmountsChange",
    },
    getTypeOptions() {
        var channel = Radio.channel("config")
        return channel.request("get:typeOptions", "tel")
    },
})
export default TelExpenseFormView
