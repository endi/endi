import Mn from "backbone.marionette"
import Radio from "backbone.radio"

import ModalBehavior from "base/behaviors/ModalBehavior.js"
import TabsBehavior from "base/behaviors/TabsBehavior.js"
import TelExpenseFormView from "./TelExpenseFormView.js"
import RegularExpenseFormView from "./ExpenseFormView.js"
import BookMarkCollectionView from "./BookMarkCollectionView.js"
import ExpenseFormPreviewWrapperView from "./ExpenseFormPreviewWrapperView"

const ExpenseFormPopupView = Mn.View.extend({
    behaviors: [
        ModalBehavior,
        {
            behaviorClass: TabsBehavior,
            tabPanes: [
                { region: "main", id: "mainform-container" },
                { region: "tel", id: "telform-container" },
                { region: "bookmark", id: "bookmark-container" },
            ],
        },
    ],
    template: require("./templates/ExpenseFormPopupView.mustache"),
    id: "expense-form-popup-modal",
    regions: {
        main: "#mainform-container",
        tel: "#telform-container",
        bookmark: "#bookmark-container",
    },
    ui: {
        main_tab: "ul.nav-tabs li.main a",
        tel_tab: "ul.nav-tabs li.tel a",
        /* override ModalBehavior.ui.modalbody selector that has no match otherwise,
        because its .modal_content_layout is outside my own template (sub-sub-template)
        */
        modalbody: ".tab-content",
        justified: "div.justified",
    },
    childViewEvents: {
        "bookmark:insert": "onBookMarkInsert",
        "success:sync": "onSuccessSync",
        "justify:form": "onJustifyForm",
    },
    // Here we bind the child FormBehavior with our ModalBehavior
    // Like it's done in the ModalFormBehavior
    childViewTriggers: {
        "cancel:form": "modal:close",
        "bookmark:delete": "bookmark:delete",
        "preview:displayStatusChange": "preview:displayStatusChange",
    },
    modelEvents: {
        "set:bookmark": "refreshForm",
    },
    initialize() {
        var facade = Radio.channel("facade")
        this.bookmarks = facade.request("get:bookmarks")
        this.add = this.getOption("add")
        this.tel = this.model.isTelType()
        const lineActions = Radio.channel("config").request(
            "get:form_section",
            "general:line_actions"
        )
        this.isEditable = lineActions.includes("edit")
    },
    onTabBeforeSelect(region) {
        let view
        switch (region._name) {
            case "main":
                view = new ExpenseFormPreviewWrapperView({
                    formView: new RegularExpenseFormView(this.viewParams),
                    model: this.viewParams.model,
                })
                break
            case "tel":
                view = new ExpenseFormPreviewWrapperView({
                    formView: new TelExpenseFormView(this.viewParams),
                    model: this.viewParams.model,
                })
                break
            case "bookmark":
                view = new BookMarkCollectionView({
                    collection: this.bookmarks,
                })
                break
        }
        this.showChildView(region._name, view)
    },
    onTabBeforeDeselect(region) {
        region.empty()
    },
    onSuccessSync() {
        if (this.add) {
            this.triggerMethod("modal:notifySuccess")
        } else {
            this.triggerMethod("modal:close")
        }
    },
    onModalAfterNotifySuccess() {
        this.triggerMethod("line:add", this, {
            category: this.model.get("category"),
        })
    },
    onModalBeforeClose() {
        this.model.rollback()
    },
    onJustifyForm(value, isChanging) {
        const shouldPostModel = isChanging && this.isEditable
        this.model
            .setJustify(value, shouldPostModel)
            .then(() =>
                this.triggerMethod("justified:change", this.model, value)
            )
    },
    refreshForm() {
        let viewParams = {
            model: this.model,
            destCollection: this.getOption("destCollection"),
            title: this.getOption("title"),
            buttonTitle: this.getOption("buttonTitle"),
            add: this.add,
            isEditable: this.isEditable,
        }
        this.viewParams = viewParams

        let activeTab = this.tel ? "tel_tab" : "main_tab"
        let activeRegion = this.getRegion(activeTab.split("_")[0])
        this.onTabBeforeSelect(activeRegion)
        this.getUI(activeTab).tab("show")
    },
    onBookMarkInsert(childView) {
        this.model.loadBookMark(childView.model)
    },
    templateContext() {
        /*
         * Form can be add form : show all tabs
         * Form can be tel form : show only the tel tab
         */
        let category_is_general = this.model.get("category") == "1"
        return {
            title: this.getOption("title"),
            add: this.add,
            allow_tel_tab: category_is_general,
            show_tel_tab: this.tel,
            show_tel: this.add || this.tel,
            show_bookmarks: this.add && this.bookmarks.length > 0,
            show_main: this.add || !this.tel,
        }
    },
    onRender: function () {
        this.refreshForm()
    },
})
export default ExpenseFormPopupView
