import Mn from "backbone.marionette"
import Radio from "backbone.radio"
import ModalBehavior from "base/behaviors/ModalBehavior.js"
import SelectWidget from "widgets/SelectWidget.js"
import { serializeForm } from "tools.js"
import { formatAmount } from "math.js"

const ExpenseDuplicateFormView = Mn.View.extend({
    behaviors: [ModalBehavior],
    id: "expense-duplicate-form",
    template: require("./templates/ExpenseDuplicateFormView.mustache"),
    regions: {
        select: ".select",
    },
    ui: {
        cancel_btn: "button[type=reset]",
        form: "form",
    },
    events: {
        "submit @ui.form": "onSubmit",
        "click @ui.cancel_btn": "onCancelClick",
    },
    initialize() {
        var channel = Radio.channel("config")
        this.options = channel.request("get:options", "expenses")
    },
    onCancelClick() {
        this.triggerMethod("modal:close")
    },
    templateContext() {
        var ht = this.model.getHT()
        var tva = this.model.getTva()
        var ttc = this.model.total()
        var is_km_fee = this.model.get("type") == "km"
        return {
            ht: formatAmount(ht),
            tva: formatAmount(tva),
            ttc: formatAmount(ttc),
            is_km_fee: is_km_fee,
        }
    },
    onRender() {
        var view = new SelectWidget({
            options: this.options,
            title: "Note de dépenses vers laquelle dupliquer",
            id_key: "id",
            field_name: "sheet_id",
        })
        this.showChildView("select", view)
    },
    onSubmit(event) {
        event.preventDefault()
        var datas = serializeForm(this.getUI("form"))
        const sheet_id = this.model.get("sheet_id")
        const facade = Radio.channel("facade")
        var request = this.model.duplicate(datas, sheet_id == datas.sheet_id)
        request.done(() => {
            let eventName = "changed:"
            if (this.model.get("type") == "km") {
                eventName += "km"
            }
            eventName += "line"

            facade.trigger(eventName)
            this.triggerMethod("modal:close")
        })
    },
})
export default ExpenseDuplicateFormView
