import Mn from "backbone.marionette"
import Radio from "backbone.radio"

import ButtonWidget from "widgets/ButtonWidget.js"
import ButtonModel from "base/models/ButtonModel.js"

const template = require("./templates/ExpenseFileItemView.mustache")

const ExpenseFileItemView = Mn.View.extend({
    regions: {
        delete_btn: ".delete-btn",
        link_btn: ".link-btn",
        new_expense_btn: ".new-expense-btn",
    },
    ui: {
        link: "a",
    },
    childViewEvents: {
        "action:clicked": "onButtonClicked",
    },
    events: {
        "click @ui.link": "onLinkClicked",
    },
    tagName: "tr",
    template: template,

    onButtonClicked(action) {
        this.trigger(`file:${action}`, this.model)
    },
    onLinkClicked(event) {
        if (this.config.request("is:previewable", this.model)) {
            this.facade.trigger("show:preview", this.model)
            event.preventDefault()
        } else {
            // let the link act normaly
            return true
        }
    },
    initialize() {
        this.facade = Radio.channel("facade")
        this.config = Radio.channel("config")
    },
    onRender() {
        if (this.getOption("edit")) {
            this.showChildView(
                "delete_btn",
                new ButtonWidget({
                    model: new ButtonModel({
                        action: "delete",
                        css: "negative icon only delete",
                        icon: "trash-alt",
                        title: "Supprimer le justificatif",
                    }),
                })
            )
            let modelLabel = this.model.get("label")

            if (this.facade.request("has:expenseLines")) {
                this.showChildView(
                    "link_btn",
                    new ButtonWidget({
                        model: new ButtonModel({
                            action: "link_to_expenseline",
                            css: "icon only",
                            icon: "paperclip",
                            title: `Lier le justificatif ${modelLabel} à une dépense`,
                        }),
                    })
                )
            }
            this.showChildView(
                "new_expense_btn",
                new ButtonWidget({
                    model: new ButtonModel({
                        action: "new_expense",
                        css: "btn-primary icon only",
                        icon: "plus",
                        title: `Ajouter une dépense liée au justificatif ${modelLabel}`,
                    }),
                })
            )
        }
    },
    templateContext() {
        return {
            edit: this.getOption("edit"),
            openLink: `/files/${this.model.get("id")}?action=download`,
            isPreviewable: this.config.request("is:previewable", this.model),
            is_achat: this.getOption("is_achat"),
            can_validate_attachments: this.getOption(
                "can_validate_attachments"
            ),
        }
    },
})
export default ExpenseFileItemView
