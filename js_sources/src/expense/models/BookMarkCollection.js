import Bb from "backbone"

import ExpenseModel from "./ExpenseModel.js"

const BookMarkCollection = Bb.Collection.extend({
    url: "/api/v1/bookmarks",
    model: ExpenseModel,
    addBookMark(model) {
        let keys = [
            "ht",
            "tva",
            "km",
            "start",
            "end",
            "description",
            "type_id",
            "category",
            "customer_id",
            "project_id",
            "business_id",
            "supplier_id",
            "invoice_number",
        ]
        var attributes = _.pick(model.attributes, ...keys)
        this.create(attributes, { wait: true })
    },
})
export default BookMarkCollection
