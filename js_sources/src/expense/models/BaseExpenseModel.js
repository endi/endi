import Bb from "backbone"
import Radio from "backbone.radio"

import BaseModel from "../../base/models/BaseModel.js"
import DuplicableMixin from "../../base/models/DuplicableMixin.js"
import { ajax_call } from "../../tools.js"

const ExpenseBaseModel = BaseModel.extend(DuplicableMixin).extend({
    /*
     * BaseModel for expenses, provides tools to access main options
     */
    getType() {
        /*
         * return the ExpenseType object associated to the current model
         */
        return this.config.request("get:ExpenseType", this.get("type_id"))
    },
    getTypeLabel() {
        /*
         * Return the Label of the current type
         */
        var current_type = this.getType()
        if (current_type === undefined) {
            return ""
        } else {
            return current_type.get("label")
        }
    },
    getCategoryLabel() {
        /*
         * Return the Label of the current type
         */
        let categories = Radio.channel("config").request(
            "get:options",
            "categories"
        )
        let category = _.find(
            categories,
            (x) => x.value == this.get("category")
        )
        if (category) {
            return category.label
        } else {
            return "INCONNU" // should not happen
        }
    },
    setJustify(value, shouldPostModel) {
        /*
         * Set the expense as justified
         */
        const url = this.url() + "?action=justified_status"
        const request = ajax_call(url, { submit: value }, "POST")
        if (shouldPostModel) {
            return request.then(() => this.save({ patch: true, wait: true }))
        } else {
            return request.then(() => this.fetch())
        }
    },
})
export default ExpenseBaseModel
