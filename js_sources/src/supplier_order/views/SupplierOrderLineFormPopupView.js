import Mn from "backbone.marionette"
import ModalBehavior from "../../base/behaviors/ModalBehavior.js"
import SupplierOrderLineFormView from "./SupplierOrderLineFormView.js"
import Radio from "backbone.radio"
// import BookMarkCollectionView from './BookMarkCollectionView.js';

const SupplierOrderLineFormPopupView = Mn.View.extend({
    behaviors: [ModalBehavior],
    id: "supplierorderline-form-popup-modal",
    template: require("./templates/SupplierOrderLineFormPopupView.mustache"),
    regions: {
        main: {
            el: ".form-child",
            replaceElement: true,
        },
    },
    ui: {
        /* override ModalBehavior.ui.modalbody selector that has no match otherwise,
        because its .modal_content_layout is outside my own template (sub-sub-template)
        */
        modalbody: "header",
    },
    childViewEvents: {
        "success:sync": "onSuccessSync",
    },
    // Here we bind the child FormBehavior with our ModalBehavior
    // Like it's done in the ModalFormBehavior
    childViewTriggers: {
        "cancel:form": "modal:close",
    },
    initialize() {
        this.add = this.getOption("add")
    },
    onSuccessSync() {
        if (this.add) {
            this.triggerMethod("modal:notifySuccess")
        } else {
            this.triggerMethod("modal:close")
        }
    },
    onModalAfterNotifySuccess() {
        this.triggerMethod("line:add")
    },
    onModalBeforeClose() {
        this.model.rollback()
    },
    refreshForm() {
        this.showForm()
    },
    showForm() {
        var view = new SupplierOrderLineFormView({
            model: this.model,
            destCollection: this.getOption("destCollection"),
            title: this.getOption("title"),
            add: this.add,
        })
        this.showChildView("main", view)
    },
    templateContext() {
        return {
            title: this.getOption("title"),
            add: this.add,
        }
    },
    onRender: function () {
        this.refreshForm()
    },
})
export default SupplierOrderLineFormPopupView
