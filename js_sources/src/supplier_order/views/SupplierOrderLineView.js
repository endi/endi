import Mn from "backbone.marionette"
import Radio from "backbone.radio"
import { formatAmount } from "../../math.js"

require("jquery-ui/ui/effects/effect-highlight")

const SupplierOrderLineView = Mn.View.extend({
    tagName: "tr",
    ui: {
        edit: "button.edit",
        delete: "button.delete",
        duplicate: "button.duplicate",
    },
    triggers: {
        "click @ui.edit": "edit",
        "click @ui.delete": "delete",
        "click @ui.duplicate": "duplicate",
    },
    modelEvents: {
        change: "render",
    },
    template: require("./templates/SupplierOrderLineView.mustache"),
    templateContext() {
        var total = this.model.total()
        let config = Radio.channel("config")
        let order_ids = config.request("get:options", "supplier_orders")
        console.log(config)
        return {
            edit: this.getOption("edit"),
            delete: this.getOption("delete"),
            duplicate: this.getOption("add") && order_ids.length > 0,
            total: formatAmount(total),
            ht_label: formatAmount(this.model.get("ht")),
            tva_label: formatAmount(this.model.get("tva")),
        }
    },
})
export default SupplierOrderLineView
