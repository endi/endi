import Mn from "backbone.marionette"
import SupplierOrderLineView from "./SupplierOrderLineView.js"
import SupplierOrderLineEmptyView from "./SupplierOrderLineEmptyView.js"

const SupplierOrderLineCollectionView = Mn.CollectionView.extend({
    tagName: "tbody",
    // Bubble up child view events
    childViewTriggers: {
        edit: "line:edit",
        delete: "line:delete",
        bookmark: "bookmark:add",
        duplicate: "line:duplicate",
    },
    childView: SupplierOrderLineView,
    emptyView: SupplierOrderLineEmptyView,
    emptyViewOptions() {
        return {
            colspan: 6,
            edit: this.getOption("section")["edit"],
        }
    },
    childViewOptions() {
        return this.getOption("section")
    },
})
export default SupplierOrderLineCollectionView
