import Validation from "backbone-validation"
import Mn from "backbone.marionette"
import SupplierOrderLineCollectionView from "./SupplierOrderLineCollectionView.js"
import Radio from "backbone.radio"
import ErrorView from "base/views/ErrorView.js"

const SupplierOrderLineTableView = Mn.View.extend({
    template: require("./templates/SupplierOrderLineTableView.mustache"),
    regions: {
        lines: {
            el: "tbody",
            replaceElement: true,
        },
        errors: ".group-errors",
    },
    ui: {
        add_btn: "button.add",
        total_ht: ".total_ht",
        total_tva: ".total_tva",
        total_ttc: ".total_ttc",
    },
    triggers: {
        "click @ui.add_btn": "line:add",
    },
    childViewTriggers: {
        "line:edit": "line:edit",
        "line:delete": "line:delete",
        "line:duplicate": "line:duplicate",
    },
    initialize(options) {
        var channel = Radio.channel("facade")
        this.totalmodel = channel.request("get:model", "total")
        this.config = Radio.channel("config")
        this.listenTo(channel, "change:lines", this.showTotals.bind(this))
        this.collection = options["collection"]
        this.listenTo(this.collection, "validated:invalid", this.showErrors)
        this.listenTo(
            this.collection,
            "validated:valid",
            this.hideErrors.bind(this)
        )
        this.listenTo(channel, "bind:validation", this.bindValidation)
        this.listenTo(channel, "unbind:validation", this.unbindValidation)
    },
    bindValidation() {
        Validation.bind(this)
    },
    unbindValidation() {
        Validation.unbind(this)
    },
    showErrors(model, errors) {
        this.detachChildView("errors")
        this.showChildView("errors", new ErrorView({ errors: errors }))
        this.$el.addClass("error")
    },
    hideErrors(model) {
        this.detachChildView("errors")
        this.$el.removeClass("error")
    },
    showTotals() {
        // this.getUI("total_ht").html(
        //     formatAmount(this.totalmodel.get('ht'))
        // );
        // this.getUI("total_tva").html(
        //     formatAmount(this.totalmodel.get('tva'))
        // );
        // this.getUI("total_ttc").html(
        //     formatAmount(this.totalmodel.get('ttc'))
        // );
    },
    templateContext() {
        return {
            edit: this.getOption("section")["edit"],
            add: this.getOption("section")["add"],
        }
    },
    onRender() {
        var view = new SupplierOrderLineCollectionView({
            collection: this.collection,
            section: this.getOption("section"),
        })
        this.showChildView("lines", view)
    },
    onAttach() {
        this.showTotals()
    },
})
export default SupplierOrderLineTableView
