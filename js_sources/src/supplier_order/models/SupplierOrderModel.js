import Radio from "backbone.radio"

import BaseModel from "../../base/models/BaseModel.js"
import DuplicableMixin from "../../base/models/DuplicableMixin.js"

const SupplierOrderModel = BaseModel.extend(DuplicableMixin).extend({
    props: ["id", "name", "cae_percentage", "supplier_id"],
    validation: {
        name: {
            required: true,
        },
        cae_percentage: {
            required: true,
        },
    },
})
export default SupplierOrderModel
