import Mn from "backbone.marionette"

const SupplierInvoiceLineEmptyView = Mn.View.extend({
    template: require("./templates/SupplierInvoiceLineEmptyView.mustache"),
    templateContext() {
        var colspan = this.getOption("colspan")
        if (this.getOption("edit")) {
            colspan += 1
        }
        return {
            colspan: colspan,
        }
    },
})
export default SupplierInvoiceLineEmptyView
