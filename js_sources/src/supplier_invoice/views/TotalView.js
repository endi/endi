import Mn from "backbone.marionette"
import Radio from "backbone.radio"

import { formatPrice, formatAmount } from "../../math.js"
import PaymentPartView from "./PaymentPartView.js"

const TotalView = Mn.View.extend({
    tagName: "div",
    template: require("./templates/TotalView.mustache"),
    modelEvents: {
        "change:ttc": "render",
        "change:ht": "render",
        "change:tva": "render",
        "change:ttc_cae": "render",
        "change:ttc_worker": "render",
    },
    regions: {
        caePartRegion: ".cae-part-region",
        workerPartRegion: ".worker-part-region",
    },
    initialize() {
        this.facade = Radio.channel("facade")
        this.supplierInvoice = this.facade.request(
            "get:model",
            "supplierInvoice"
        )
    },
    onRender() {
        if (this.supplierInvoice.get("internal")) {
            var view = new PaymentPartView({
                payments: this.supplierInvoice.get("payments"),
                payment_wording: "paiement",
                payments_recipient: this.supplierInvoice.get("supplier_name"),
            })
            this.showChildView("workerPartRegion", view)
        } else {
            let cae_percentage = this.supplierInvoice.get("cae_percentage")
            if (cae_percentage > 0) {
                var view = new PaymentPartView({
                    payments: this.supplierInvoice.get("cae_payments"),
                    payment_wording: "paiement",
                    payments_recipient:
                        this.supplierInvoice.get("supplier_name"),
                })
                this.showChildView("caePartRegion", view)
            }
            if (cae_percentage < 100) {
                var view = new PaymentPartView({
                    payments: this.supplierInvoice.get("user_payments"),
                    payment_wording: "remboursement",
                    payments_recipient: this.supplierInvoice.get("payer_name"),
                })
                this.showChildView("workerPartRegion", view)
            }
        }
    },
    templateContext() {
        let orders_total = this.supplierInvoice.get("orders_total")
        let invoice_total = this.model.get("ttc")
        let totals_mismatch = orders_total != invoice_total && orders_total > 0

        return {
            ht: formatAmount(this.model.get("ht")),
            tva: formatAmount(this.model.get("tva")),
            ttc: formatAmount(this.model.get("ttc")),
            ttc_cae: formatAmount(this.model.get("ttc_cae")),
            ttc_worker: formatAmount(this.model.get("ttc_worker")),
            orders_ttc_worker: formatAmount(
                this.supplierInvoice.get("orders_worker_total")
            ),
            orders_ttc_cae: formatAmount(
                this.supplierInvoice.get("orders_cae_total")
            ),
            orders_ht: formatAmount(
                this.supplierInvoice.get("orders_total_ht")
            ),
            orders_tva: formatAmount(
                this.supplierInvoice.get("orders_total_tva")
            ),
            orders_ttc: formatAmount(this.supplierInvoice.get("orders_total")),
            cae_percentage: Number(this.supplierInvoice.get("cae_percentage")),
            worker_percentage: 100 - this.supplierInvoice.get("cae_percentage"),

            totals_mismatch: totals_mismatch,
            payments: this.supplierInvoice.get("payments"),
        }
    },
})
export default TotalView
