import SupplierInvoiceLineModel from "./SupplierInvoiceLineModel.js"
import BaseLineCollection from "../../base/models/BaseLineCollection.js"

const SupplierInvoiceLineCollection = BaseLineCollection.extend({
    model: SupplierInvoiceLineModel,
    validate: function () {
        var result = {}
        this.each(function (model) {
            var res = model.validate()
            if (res) {
                _.extend(result, res)
            }
        })
        if (this.models.length == 0) {
            result["lines"] = "Veuillez ajouter au moins un produit"
            this.trigger("validated:invalid", this, {
                lines: "Veuillez ajouter au moins un produit",
            })
        }
        return result
    },
})
export default SupplierInvoiceLineCollection
