import Bb from "backbone"
import Mn from "backbone.marionette"
import { hideLoader } from "tools.js"
import RootComponent from "../views/RootComponent.js"
import Controller from "./Controller.js"
import Router from "./Router.js"

const AppClass = Mn.Application.extend({
    region: "#js-main-area",
    onBeforeStart(app, options) {
        console.log("AppClass.onBeforeStart")
        this.rootView = new RootComponent()
        this.controller = new Controller({
            rootView: this.rootView,
        })
        this.router = new Router({
            controller: this.controller,
        })
        console.log("AppClass.onBeforeStart finished")
    },
    onStart(app, options) {
        this.showView(this.rootView)
        console.log("Starting the history")
        hideLoader()
        Bb.history.start()
    },
})
const App = new AppClass()
export default App
