import Radio from "backbone.radio"
import Mn from "backbone.marionette"
import FileContentView from "./FileContentView"
import _ from "underscore"

/**
 * File component grouping File view management
 *
 * Presents a block with
 *
 * - File requirements
 * - Other attached files
 *
 * Handles its own refresh process
 */
const FileComponent = Mn.View.extend({
    template: _.template('<div class="files collapsible in"></div>'),
    regions: {
        content: ".files",
    },
    childViewEvents: {
        "file:updated": "refresh",
    },
    initialize(options) {
        const facade = Radio.channel("facade")
        this.collection = facade.request("get:collection", "file_requirements")
        this.other_files_collection = facade.request(
            "get:collection",
            "attached_files"
        )
    },
    refresh() {
        $.when(
            this.collection.fetch(),
            this.other_files_collection.fetch()
        ).then(() => this.render())
    },
    onRender() {
        const view = new FileContentView({
            collection: this.collection,
            other_files_collection: this.other_files_collection,
        })
        this.showChildView("content", view)
    },
})
export default FileComponent
