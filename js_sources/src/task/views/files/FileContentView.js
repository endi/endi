import Mn from "backbone.marionette"
import { Radio } from "backbone"
import FileView from "./FileView"
import FileCollectionView from "./FileCollectionView"
import { FileRequirementView, FileOptionnalView } from "./FileRequirementView"

const FileContentView = Mn.View.extend({
    tagName: "div",
    template: require("./templates/FileContentView.mustache"),
    regions: {
        required_files: {
            el: ".required-files tbody",
            replaceElement: true,
        },
        optionnal_files: {
            el: ".optionnal-files tbody",
            replaceElement: true,
        },
        other_files: {
            el: ".other-files tbody",
            replaceElement: true,
        },
    },
    ui: {
        addFileButton: ".add-file-btn",
    },
    events: {
        "click @ui.addFileButton": "onFileAdd",
    },
    childViewTriggers: {
        "file:updated": "file:updated",
    },
    /**
     *  NB : className() runs before initialize
     *
     * @returns the css class of the View's el
     */
    className: function () {
        let result = "separate_block border_left_block"
        this.facade = Radio.channel("facade")
        this.has_warning = this.facade.request("has:filewarning")
        if (this.has_warning) {
            result += " error"
        }
        return result
    },

    onRender() {
        const requirement_filter = (model) =>
            model.get("requirement_type") != "optionnal"
        if (this.collection.filter(requirement_filter).length > 0) {
            var view = new FileCollectionView({
                collection: this.collection,
                childView: FileRequirementView,
                requirement_filter: requirement_filter,
            })
            this.showChildView("required_files", view)
        }
        const optionnal_filter = (model) =>
            model.get("requirement_type") == "optionnal"
        if (this.collection.filter(optionnal_filter).length > 0) {
            var view = new FileCollectionView({
                collection: this.collection,
                childView: FileOptionnalView,
                requirement_filter: optionnal_filter,
            })
            this.showChildView("optionnal_files", view)
        }

        view = new FileCollectionView({
            collection: this.getOption("other_files_collection"),
            childView: FileView,
        })
        this.showChildView("other_files", view)
    },
    templateContext() {
        const requirement_filter = (model) =>
            model.get("requirement_type") != "optionnal"
        const optionnal_filter = (model) =>
            model.get("requirement_type") == "optionnal"

        return {
            has_warning: this.has_warning,
            has_required_files:
                this.collection.filter(requirement_filter).length > 0,
            has_optionnal_files:
                this.collection.filter(optionnal_filter).length > 0,
            has_other_files:
                this.getOption("other_files_collection").length > 0,
        }
    },
    onFilePopupCallback() {
        this.triggerMethod("file:updated")
    },
    onFileAdd() {
        const url = AppOption["file_upload_url"]
        window.openPopup(url, this.onFilePopupCallback.bind(this))
    },
})
export default FileContentView
