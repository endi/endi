import Mn from "backbone.marionette"
import Radio from "backbone.radio"
import { formatAmount } from "../../../math"
const template = require("./templates/Details.mustache")
const Details = Mn.View.extend({
    template: template,
    initialize() {
        this.data = this.getOption("data")
    },
    templateContext() {
        // Collect data sent to the template (model attributes are already transmitted)
        const flat_cost = this.data["flat_cost"]
        const general_overhead = this.data["general_overhead"]
        const margin = this.data["margin"]
        const contribution = this.data["contribution"]
        return {
            flat_cost: formatAmount(flat_cost, true),
            general_overhead: formatAmount(general_overhead, true),
            margin: formatAmount(margin, true),
            total_ht: formatAmount(this.data["total_ht"], true),
            has_hours: "hours" in this.data,
            hours: this.data["hours"],
            title: this.getOption("title"),
            label: this.getOption("label"),
        }
    },
})
export default Details
