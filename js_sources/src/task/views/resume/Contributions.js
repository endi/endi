import Mn from "backbone.marionette"
import Radio from "backbone.radio"
import { formatAmount } from "../../../math"
const template = require("./templates/Contributions.mustache")
const Contributions = Mn.View.extend({
    template: template,
    regions: {},
    ui: {},
    events: {},
    childViewEvents: {},
    childViewTriggers: {},
    initialize() {
        this.config = Radio.channel("config")
        this.facade = Radio.channel("facade")
        this.data = this.getOption("data")
    },
    templateContext() {
        // Collect data sent to the template (model attributes are already transmitted)
        const insurance = this.data.insurance || 0
        const contribution = this.data.cae || 0

        const has_insurance = insurance && insurance != 0
        const has_contribution = contribution && contribution != 0
        return {
            insurance: formatAmount(insurance, true),
            has_insurance: has_insurance,
            has_contribution: has_contribution,
            contribution: formatAmount(contribution, true),
            total: formatAmount(contribution + insurance, true),
        }
    },
})
export default Contributions
