import Mn from "backbone.marionette"
const template = require("./templates/RelatedEstimationView.mustache")

const RelatedEstimationView = Mn.View.extend({
    template: template,
    tagName: "span",
})

const RelatedEstimationCollectionView = Mn.CollectionView.extend({
    template: require("./templates/RelatedEstimationCollectionView.mustache"),
    childView: RelatedEstimationView,
    className: "separate_bottom content_vertical_padding",
    childViewContainer: ".children",
    templateContext() {
        return { multiple: this.collection.length > 1 }
    },
})
export default RelatedEstimationCollectionView
