import Mn from "backbone.marionette"
import Radio from "backbone.radio"

import FormBehavior from "base/behaviors/FormBehavior.js"
import CheckboxWidget from "widgets/CheckboxWidget"
import { showLoader } from "tools"

const DisplayOptionsView = Mn.View.extend({
    /**
     * Display DisplayUnitsView and DisplayTTCView
     */

    template: require("./templates/DisplayOptionsView.mustache"),
    tagName: "div",
    className: "form-section",
    behaviors: [FormBehavior],
    regions: {
        display_units_container: ".display-units-container",
        display_ttc_container: ".display-ttc-container",
        input_mode_container: ".input-mode-container",
    },
    childViewEvents: {
        inputModeChange: "onInputModeChange",
    },
    childViewTriggers: {
        finish: "data:persist",
    },
    initialize(options) {
        this.section = options["section"]
    },
    showInputMode() {
        this.showChildView(
            "input_mode_container",
            new CheckboxWidget({
                field_name: "input_mode",
                title: "",
                inline_label: "Étude de prix",
                description:
                    "Le calcul se base sur le cout d’achat HT et les coefficients de frais généraux et de marge.",
                finishEventName: "inputModeChange",
                true_val: "price_study",
                false_val: "classic",
                value: this.model.get("input_mode"),
            })
        )
    },
    showDisplayUnits() {
        const view = new CheckboxWidget({
            title: "",
            inline_label:
                "Afficher le détail (prix unitaire et quantité) des produits dans le PDF",
            description:
                "Les informations qui seront masquées dans le PDF sont hachurées.",
            field_name: "display_units",
            value: this.model.get("display_units"),
        })
        this.showChildView("display_units_container", view)
    },
    showDisplayTtc() {
        const view = new CheckboxWidget({
            title: "",
            inline_label: "Afficher les prix TTC dans le PDF",
            field_name: "display_ttc",
            value: this.model.get("display_ttc"),
        })
        this.showChildView("display_ttc_container", view)
    },
    onRender() {
        if (this.section["input_mode_edit"]) {
            this.showInputMode()
        }

        if (this.section["display_units"]) {
            this.showDisplayUnits()
        }
        if (this.section["display_ttc"]) {
            this.showDisplayTtc()
        }
    },
    onInputModeChange(field_name, value) {
        let confirm = true
        if (value == "classic") {
            confirm = window.confirm(
                "Le détail des calculs de l'étude de prix seront perdus, continuez ?"
            )
        } else if (value == "price_study") {
            confirm = window.confirm(
                "Les prestations déjà saisies seront supprimées"
            )
        }
        if (confirm) {
            showLoader()
            this.triggerMethod("data:persist", field_name, value)
        } else {
            this.showInputMode()
        }
    },
})

export default DisplayOptionsView
