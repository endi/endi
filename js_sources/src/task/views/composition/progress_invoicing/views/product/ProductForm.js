/*
 * Module name : ProductForm
 */
import Mn from "backbone.marionette"

import InputWidget from "widgets/InputWidget.js"
import Radio from "backbone.radio"
import ModalFormBehavior from "base/behaviors/ModalFormBehavior.js"

const template = require("./templates/ProductForm.mustache")

const ProductForm = Mn.View.extend({
    template: template,
    behaviors: [ModalFormBehavior],
    bb_sync: true,
    partial: true,
    regions: {
        percent_done: ".percent_done",
        percent_left: ".percent_left",
        current_percent: ".current_percent",
    },
    initialize() {
        this.app = Radio.channel("progressInvoicingApp")
    },
    refreshForm: function () {
        this.showChildView(
            "percent_done",
            new InputWidget({
                value: this.model.get("already_invoiced"),
                field_name: "_percent_done",
                label: "Déjà facturé",
                editable: false,
            })
        )
        this.showChildView(
            "current_percent",
            new InputWidget({
                value: this.model.get("percentage"),
                title: "À facturer",
                field_name: "percentage",
                addon: "€",
            })
        )
        this.showChildView(
            "percent_left",
            new InputWidget({
                value: this.model.get("percent_left"),
                field_name: "_percent_left",
                label: "Restera à facturer",
                editable: false,
            })
        )
    },
    onRender: function () {
        this.refreshForm()
    },
    templateContext: function () {
        let deposit_info = false
        if (this.model.get("has_deposit")) {
            deposit_info = true
        }
        return {
            popup_title: "Pourcentage à facturer",
            total_ht_to_invoice_label: this.model.total_ht_to_invoice_label(),
            tva_to_invoice_label: this.model.tva_to_invoice_label(),
            total_ttc_to_invoice_label: this.model.total_ttc_to_invoice_label(),
            deposit_info: deposit_info,
        }
    },
    onSuccessSync() {
        this.app.trigger("product:changed", this.model)
    },
})
export default ProductForm
