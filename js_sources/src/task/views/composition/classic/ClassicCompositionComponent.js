/*
 * Module name : CompositionComponent
 */
import Mn from "backbone.marionette"
import Radio from "backbone.radio"
import LinesComponent from "./lines/LinesComponent.js"
import HtBeforeDiscountsView from "./discount/HtBeforeDiscountsView.js"
import DiscountComponent from "./discount/DiscountComponent.js"
import HtAfterDiscountsView from "./discount/HtAfterDiscountsView.js"
import TotalTTCView from "./post_ttc_line/TotalTTCView.js"
import PostTTCLineComponent from "./post_ttc_line/PostTTCLineComponent.js"
import TotalDueView from "./post_ttc_line/TotalDueView.js"
import ExpenseHtComponent from "./ExpenseHtComponent.js"

const template = require("./templates/CompositionComponent.mustache")

/**
 * Main Composition View
 *
 * Displays the classic task line edition mode
 *
 */

const ClassicCompositionComponent = Mn.View.extend({
    template: template,
    regions: {
        tasklines: "#tasklines",
        ht_before_discounts: ".ht-before-discounts",
        discounts: "#discounts",
        ht_after_discounts: ".ht-after-discounts",
        total_ttc: ".total-ttc",
        post_ttc_lines: "#post_ttc_lines",
        total_due: ".total-due",
        expenses_ht: "#expenses_ht",
        link_container: ".link-container",
    },
    ui: {},
    // Listen to the current's view events
    events: {},
    // Listen to child view events
    childViewEvents: {},
    // Bubble up child view events
    childViewTriggers: {},
    initialize(options) {
        this.config = Radio.channel("config")
        this.facade = Radio.channel("facade")
        this.section_options = options["section"]
        this.totalmodel = options["totalmodel"]
        this.edit = options["edit"]
    },
    onRender() {
        if (this.section_options.hasOwnProperty("lines")) {
            this.showLinesComponent()
        }
        if (this.section_options.hasOwnProperty("discounts")) {
            let view_ht_before = new HtBeforeDiscountsView({
                model: this.totalmodel,
            })
            this.showChildView("ht_before_discounts", view_ht_before)
            this.showDiscountComponent()
            let view_ht_after = new HtAfterDiscountsView({
                model: this.totalmodel,
            })
            this.showChildView("ht_after_discounts", view_ht_after)
        }
        if (this.section_options.hasOwnProperty("post_ttc_lines")) {
            let view_ttc = new TotalTTCView({
                model: this.totalmodel,
            })
            this.showChildView("total_ttc", view_ttc)
            this.showPostTTCLineComponent()
            let view_due = new TotalDueView({
                model: this.totalmodel,
            })
            this.showChildView("total_due", view_due)
        }
        if (this.section_options.hasOwnProperty("expenses_ht")) {
            this.showExpenseHtBlock()
        }
    },
    showLinesComponent() {
        var lineSection = this.section_options["lines"]
        var model = this.facade.request("get:model", "common")
        var collection = this.facade.request("get:collection", "task_groups")
        var view = new LinesComponent({
            collection: collection,
            edit: this.edit,
            model: model,
            section: lineSection,
        })
        this.showChildView("tasklines", view)
    },
    showDiscountComponent: function () {
        var section = this.section_options["discounts"]
        var collection = this.facade.request("get:collection", "discounts")
        var view = new DiscountComponent({
            collection: collection,
            edit: this.edit,
            section: section,
        })
        this.showChildView("discounts", view)
    },
    showPostTTCLineComponent: function () {
        var section = this.section_options["post_ttc_lines"]
        var collection = this.facade.request("get:collection", "post_ttc_lines")
        var view = new PostTTCLineComponent({
            collection: collection,
            edit: this.edit,
            section: section,
        })
        this.showChildView("post_ttc_lines", view)
    },
    showExpenseHtBlock: function () {
        var model = this.facade.request("get:model", "expense_ht")
        var view = new ExpenseHtComponent({
            model: model,
        })
        this.showChildView("expenses_ht", view)
    },
})
export default ClassicCompositionComponent
