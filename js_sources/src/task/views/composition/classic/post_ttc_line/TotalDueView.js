import Mn from "backbone.marionette"
import { formatAmount } from "../../../../../math.js"
import LabelRowWidget from "../../../../../widgets/LabelRowWidget.js"

const TotalDueView = Mn.View.extend({
    className: "table_container",
    template: require("./templates/TotalDueView.mustache"),
    regions: {
        line: ".line",
    },
    modelEvents: {
        change: "render",
    },
    onRender: function () {
        var values = formatAmount(this.model.get("total_due"), true)
        var view = new LabelRowWidget({
            label: "Net à payer",
            values: values,
            colspan: 5,
        })
        this.showChildView("line", view)
    },
})
export default TotalDueView
