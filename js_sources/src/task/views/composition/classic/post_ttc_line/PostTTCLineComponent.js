import Mn from "backbone.marionette"
import Radio from "backbone.radio"
import PostTTCLineModel from "../../../../models/PostTTCLineModel.js"
import PostTTCLineCollectionView from "./PostTTCLineCollectionView.js"
import PostTTCLineFormPopupView from "./PostTTCLineFormPopupView.js"
import ErrorView from "base/views/ErrorView.js"
import { displayServerSuccess, displayServerError } from "backbone-tools.js"

const PostTTCLineComponent = Mn.View.extend({
    tagName: "div",
    className: "form-section post_ttc_line-group",
    template: require("./templates/PostTTCLineComponent.mustache"),
    regions: {
        lines: {
            el: ".lines",
            replaceElement: true,
        },
        modalRegion: ".modalregion",
        errors: ".block-errors",
    },
    ui: {
        add_button: "button.btn-add",
    },
    triggers: {
        "click @ui.add_button": "line:add",
    },
    childViewEvents: {
        "line:edit": "onLineEdit",
        "line:delete": "onLineDelete",
        "destroy:modal": "render",
    },
    collectionEvents: {
        change: "hideErrors",
        "validated:invalid": "showErrors",
        "validated:valid": "hideErrors",
        sync: "render",
    },
    initialize: function (options) {
        this.collection = options["collection"]
        this.listenTo(this.collection, "validated:invalid", this.showErrors)
        this.listenTo(
            this.collection,
            "validated:valid",
            this.hideErrors.bind(this)
        )
        this.edit = options["edit"]
    },
    showErrors(model, errors) {
        this.detachChildView("errors")
        this.showChildView(
            "errors",
            new ErrorView({
                errors: errors,
            })
        )
        this.$el.addClass("error")
    },
    hideErrors(model) {
        this.detachChildView("errors")
        this.$el.removeClass("error")
    },
    isEmpty: function () {
        return this.collection.length === 0
    },
    onLineAdd: function () {
        var model = new PostTTCLineModel()
        this.showPostTTCLineForm(
            model,
            "Ajouter un ajustement du total TTC",
            false
        )
    },
    onLineEdit: function (childView) {
        this.showPostTTCLineForm(
            childView.model,
            "Modifier un ajustement du total TTC",
            true
        )
    },
    showPostTTCLineForm: function (model, title, edit) {
        var form = new PostTTCLineFormPopupView({
            model: model,
            title: title,
            destCollection: this.collection,
            edit: edit,
        })
        this.showChildView("modalRegion", form)
    },
    onDeleteSuccess: function () {
        displayServerSuccess("Vos données ont bien été supprimées")
    },
    onDeleteError: function () {
        displayServerError(
            "Une erreur a été rencontrée lors de la " +
                "suppression de cet élément"
        )
    },
    onLineDelete: function (childView) {
        var result = window.confirm(
            "Êtes-vous sûr de vouloir supprimer cet ajustement ?"
        )
        if (result) {
            childView.model.destroy({
                success: this.onDeleteSuccess,
                error: this.onDeleteError,
            })
        }
    },
    templateContext: function () {
        const empty = this.isEmpty()
        return {
            not_empty: !empty,
            collapsed: empty,
            edit: this.edit,
        }
    },
    onRender: function () {
        if (!this.isEmpty()) {
            this.showChildView(
                "lines",
                new PostTTCLineCollectionView({
                    collection: this.collection,
                    edit: this.edit,
                })
            )
        }
    },
})
export default PostTTCLineComponent
