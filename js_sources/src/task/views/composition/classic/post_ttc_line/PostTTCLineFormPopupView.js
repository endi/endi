import Mn from "backbone.marionette"
import ModalBehavior from "../../../../../base/behaviors/ModalBehavior.js"
import PostTTCLineFormView from "./PostTTCLineFormView.js"
import { getOpt } from "../../../../../tools.js"

var template = require("./templates/PostTTCLineFormPopupView.mustache")

const PostTTCLineFormPopupView = Mn.View.extend({
    id: "post_ttc_line-form-popup",
    behaviors: [ModalBehavior],
    template: template,
    regions: {
        "simple-form": ".simple-form",
    },
    // Here we bind the child FormBehavior with our ModalBehavior
    // Like it's done in the ModalFormBehavior
    childViewTriggers: {
        "cancel:form": "modal:close",
        "success:sync": "modal:close",
    },
    onModalBeforeClose() {
        this.model.rollback()
    },
    isAddView: function () {
        return !getOpt(this, "edit", false)
    },
    onRender: function () {
        this.showChildView(
            "simple-form",
            new PostTTCLineFormView({
                model: this.model,
                title: this.getOption("title"),
                destCollection: this.getOption("destCollection"),
            })
        )
    },
    templateContext: function () {
        return {
            title: this.getOption("title"),
            add: this.isAddView(),
        }
    },
})
export default PostTTCLineFormPopupView
