import _ from "underscore"
import Mn from "backbone.marionette"
import Radio from "backbone.radio"

const template = require("./templates/PostTTCLineView.mustache")

const PostTTCLineView = Mn.View.extend({
    template: template,
    tagName: "tr",
    ui: {
        edit_button: "button.edit",
        delete_button: "button.delete",
    },
    // Trigger to the parent
    triggers: {
        "click @ui.edit_button": "edit",
        "click @ui.delete_button": "delete",
    },
    modelEvents: {
        change: "render",
    },
    initialize() {
        var channel = Radio.channel("config")
    },
    templateContext() {
        return {
            edit: this.getOption("edit"),
            amount_label: this.model.amount_label(),
        }
    },
})
export default PostTTCLineView
