import Mn from "backbone.marionette"
import { formatAmount } from "../../../../../math.js"
import LabelRowWidget from "../../../../../widgets/LabelRowWidget.js"

const HtAfterDiscountsView = Mn.View.extend({
    className: "table_container",
    template: require("./templates/HtAmountsDiscountsView.mustache"),
    regions: {
        line: ".line",
    },
    modelEvents: {
        change: "render",
    },
    onRender: function () {
        var values = formatAmount(this.model.get("ht"), true)
        var view = new LabelRowWidget({
            label: "Total HT",
            values: values,
            colspan: 5,
        })
        this.showChildView("line", view)
    },
})
export default HtAfterDiscountsView
