/*
 * Module name : RootComponent
 */
import Mn from "backbone.marionette"

import DiscountForm from "./discount/DiscountForm.js"
import ProductForm from "./product/ProductForm.js"
import BlockView from "./BlockView.js"
import WorkForm from "./workform/WorkForm.js"
import ChapterForm from "./chapter/ChapterForm.js"
import WorkItemForm from "./workform/WorkItemForm.js"

const template = require("./templates/RootComponent.mustache")

const RootComponent = Mn.View.extend({
    className: "",
    template: template,
    regions: {
        main: ".main",
        modalContainer: ".modal-container",
    },
    initialize(options) {
        this.initialized = false
        this.section = options["section"]
    },
    index() {
        this.initialized = true
        const view = new BlockView({
            model: this.model,
            collection: this.collection,
            section: this.section,
        })
        this.showChildView("main", view)
    },
    showModal(view) {
        this.showChildView("modalContainer", view)
    },
    _showFormInModal(FormClass, model, collection, edit) {
        this.index()
        const view = new FormClass({
            model: model,
            destCollection: collection,
            edit: edit,
        })
        this.showModal(view)
    },
    _showChapterForm(model, collection, edit) {
        this._showFormInModal(ChapterForm, model, collection, edit)
    },
    showAddChapterForm(model, collection) {
        this._showChapterForm(model, collection, false)
    },
    showEditChapterForm(model, collection) {
        this._showChapterForm(model, collection, true)
    },
    _showProductForm(model, collection, edit) {
        this._showFormInModal(ProductForm, model, collection, edit)
    },
    showAddProductForm(model, collection) {
        this._showProductForm(model, collection, false)
    },
    showEditProductForm(model) {
        this._showProductForm(model, model.collection, true)
    },
    _showDiscountForm(model, collection, edit) {
        this._showFormInModal(DiscountForm, model, collection, edit)
    },
    showAddDiscountForm(model, collection) {
        this._showDiscountForm(model, collection, false)
    },
    showEditDiscountForm(model) {
        this._showDiscountForm(model, model.collection, true)
    },
    _showWorkForm(model, collection, edit) {
        this._showFormInModal(WorkForm, model, collection, edit)
    },
    showAddWorkForm(model, collection) {
        this._showWorkForm(model, collection, false)
    },
    showEditWorkForm(model) {
        this._showWorkForm(model, model.collection, true)
    },
    _showWorkItemForm(model, collection, edit) {
        this._showFormInModal(WorkItemForm, model, collection, edit)
    },
    showAddWorkItemForm(model, collection) {
        this._showWorkItemForm(model, collection, false)
    },
    showEditWorkItemForm(model) {
        this._showWorkItemForm(model, model.collection, true)
    },
})
export default RootComponent
