import AppRouter from "marionette.approuter"

const Router = AppRouter.extend({
    appRoutes: {
        "": "index",
        index: "index",
        addchapter: "addChapter",
        "chapters/:id": "editChapter",
        "chapters/:id/addproduct": "addProduct",
        "chapters/:id/products/:id": "editProduct",
        "chapters/:id/addwork": "addWork",
        "chapters/:id/works/:id": "editWork",
        adddiscount: "addDiscount",
        "discounts/:id": "editDiscount",
    },
})
export default Router
