import PriceStudyApp from "./price_study/App.js"
import ProgressInvoicingApp from "./progress_invoicing/App"
import ClassicCompositionComponent from "./classic/ClassicCompositionComponent"

export default function showCompositionComponent(region, section, totalmodel) {
    if (section["mode"] == "price_study") {
        const subapp = new PriceStudyApp({
            region: region,
        })
        subapp.start({
            section: section["price_study"],
            edit: section["edit"],
        })
    } else if (section["mode"] == "progress_invoicing") {
        const subapp = new ProgressInvoicingApp({
            region: region,
        })
        subapp.start({
            section: section["progress_invoicing"],
            edit: section["edit"],
        })
    } else {
        const view = new ClassicCompositionComponent({
            section: section["classic"],
            edit: section["edit"],
            totalmodel: totalmodel,
        })
        region.show(view)
    }
}
