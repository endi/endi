/* global AppOption; */
import $ from "jquery"

import { applicationStartup } from "../backbone-tools.js"
import App from "./components/App.js"
import Facade from "./components/Facade.js"
import ToolbarApp from "./components/ToolbarApp.js"
import UserPreferences from "./components/UserPreferences.js"
import PreviewService from "../common/components/PreviewService"

$(function () {
    applicationStartup(AppOption, App, Facade, {
        actionsApp: ToolbarApp,
        customServices: [PreviewService, UserPreferences],
    })
})
