import BaseModel from "base/models/BaseModel"
import _ from "underscore"
import ProductCollection from "./ProductCollection"

const ChapterModel = BaseModel.extend({
    // Define the props that should be set on your model
    props: [
        "id",
        "title",
        "description",
        "order",
        "products",
        "total_ht",
        "display_details",
    ],
    defaults: {
        display_details: true,
    },
    validation: {
        products: function (value) {
            if (this.products.length == 0) {
                return `Chapitre ${this.get("title")} : Veuillez saisir au moins un produit`
            }
        },
    },
    initialize(options) {
        ChapterModel.__super__.initialize.apply(this, arguments)
        this.populate()
        this.on("change:products", () => this.populate())
    },
    populate() {
        // On ne crée la "sous-collection" que si le modèle ici a déjà un id
        if (this.get("id")) {
            if (!this.products) {
                this.products = new ProductCollection([], {
                    url: this.url() + "/" + "products",
                })
                this.products._parent = this
            }
            this.products.fetch()
        }
    },
    validateModel() {
        // Valide la présence de produit dans le chapitre
        if (this.products.length == 0) {
            // this.trigger('validated:invalid', this, {}});
            return {
                Produits: "Veuillez saisir au moins un produit",
            }
        }
        // Valide les produits (compte tenu que l'on présente les vues sans
        // Backbone, on a besoin de trigger les erreurs de validation au niveau
        // de ce modèle)
        const childValidation = this.products.validate()
        if (childValidation && !_.isEmpty(childValidation)) {
            // this.trigger('validated:invalid', this, childValidation);
            // Ici on return car le validate ci-dessous risque de trigger
            // un valideted:valid qui va cacher les erreurs qu'on a trigger ici
            return {
                Produits: "Produits : il manque des informations requises",
            }
        }
        // Si le reste est valide, on valide le modèle courant
        return this.validate()
    },
})
export default ChapterModel
