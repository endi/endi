import BaseModel from "base/models/BaseModel"
import Radio from "backbone.radio"

const RelatedEstimationModel = BaseModel.extend({
    // Marque ce modèle comme local pour qu'il ne soit pas sauvegardé automatiquement
    isLocalModel: true,
    props: ["id", "label", "url"],
})
export default RelatedEstimationModel
