import Bb from "backbone"
import FileRequirementModel from "./FileRequirementModel.js"

const FileRequirementCollection = Bb.Collection.extend({
    model: FileRequirementModel,
    url: function () {
        return AppOption["context_url"] + "/" + "file_requirements"
    },
    validate(status) {
        var result = true
        this.each(function (model) {
            var res = model.validate(status)
            if (!res) {
                result = false
            }
        })
        return result
    },
})
export default FileRequirementCollection
