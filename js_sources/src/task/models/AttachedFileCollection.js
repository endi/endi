import Bb from "backbone"
import NodeFileModel from "../../common/models/NodeFileModel"

const AttachedFileCollection = Bb.Collection.extend({
    model: NodeFileModel,
    /**
     * Custom parse method
     * Traite les données renvoyées par le serveur pour construire la collection
     */
    parse(xhrResponse) {
        if ("attachments" in xhrResponse) {
            return xhrResponse["attachments"]
        } else {
            return xhrResponse
        }
    },
})
export default AttachedFileCollection
