import BaseCollection from "base/models/BaseCollection.js"
import ChapterModel from "./ChapterModel"

const ChapterCollection = BaseCollection.extend({
    // Define the props that should be set on your model
    model: ChapterModel,
})
export default ChapterCollection
