import _ from "underscore"
import BaseModel from "../../base/models/BaseModel.js"
import Radio from "backbone.radio"

const GeneralModel = BaseModel.extend({
    props: ["id", "name", "business_type_id"],
    validation: {
        name: {
            required: true,
            msg: "Veuillez saisir un nom",
        },
    },
    initialize: function () {
        GeneralModel.__super__.initialize.apply(this, arguments)
        this.config_channel = Radio.channel("config")
    },
    getBusinessType() {
        this.business_type_options = this.config_channel.request(
            "get:options",
            "business_types"
        )
        var business_type = _.findWhere(this.business_type_options, {
            value: this.get("business_type_id"),
        })
        return business_type
    },
})
export default GeneralModel
