import Mn from "backbone.marionette"
import Radio from "backbone.radio"

/** A <form action=post> with a single action button
 *
 * This is for actions that target POST views. Visually, it looks like the same
 * as AnchorWidget, that handle GET views.
 */
const POSTButtonWidget = Mn.View.extend({
    tagName: "div",
    template: require("./templates/POSTButtonWidget.mustache"),
    ui: {
        button: "button",
    },
    events: {
        "click @ui.button": "confirm",
    },
    confirm() {
        let options = this.model.get("option")
        if (options.confirm_msg) {
            return window.confirm(options.confirm_msg)
        } else {
            return true
        }
    },
    templateContext() {
        let channel = Radio.channel("config")
        let csrf_token = channel.request("get:options", "csrf_token")
        return { csrf_token: csrf_token }
    },
    redirect(data, textStatus, jqXHR) {
        window.location = jqXHR.getResponseHeader("Location")
    },
    refresh(data, textStatus, jqXHR) {
        console.log("REFRESH", jqXHR, textStatus)
        window.location.reload()
    },
})

export default POSTButtonWidget
