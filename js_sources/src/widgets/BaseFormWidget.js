/*
 *
 */
import Mn from "backbone.marionette"
import { getOpt } from "../tools.js"

/**
 * @param {string} title - The Field UI label
 * @param {string} label - Other way of setting the Field UI label
 * @param {string} changeEventName - The base name of the event triggerd on
 * data change (blur event) (default: 'change')
 * @param {string} finishEventName - The base name of the event triggerd on
 * data finish (leaving the field) (default 'finish')
 * @param {string} field_name - The form field name (attribute name)
 * @param {string} ariaLabel - Custom aria-label (default to title/label)
 * @param {string} description - The field description (under the input/select...)
 * @param {boolean} editable - Is it readonly (defaul true)
 * @param {boolean} required - Is it a mandatory field (will add a star to the UI label)
 * @param {string} value - The value to set (string/date/integer depending on the Child Class)
 *
 * Trigger tools
 *
 * @method triggerChange: Trigger the change and change:field_name events with the value
 * @method triggerFinish: Trigger the finish and finish:field_name events with the value
 */

const BaseFormWidget = Mn.View.extend({
    initialize() {
        this.cid = _.uniqueId()
        this.currentValue = ""
    },
    getTagId: function () {
        /*
         * Return an id for the current textarea
         */
        var id_ = getOpt(this, "field_id", false)
        if (!id_) {
            var field_name = this.getOption("field_name")
            id_ = field_name + "-" + this.cid
        }
        return id_
    },
    getCurrentValue() {
        return this.currentValue
    },
    triggerChange(field_value) {
        const event_name = getOpt(this, "changeEventName", "change")
        const field_name = this.getOption("field_name")
        this.triggerMethod(event_name, field_name, field_value)
        this.triggerMethod(event_name + ":" + field_name, field_value)
        this.currentValue = field_value
    },
    triggerFinish(field_value) {
        const event_name = getOpt(this, "finishEventName", "finish")
        console.log("Triggering %s, %s", event_name, field_value)
        const field_name = this.getOption("field_name")
        this.triggerMethod(event_name, field_name, field_value)
        this.triggerMethod(event_name + ":" + field_name, field_value)
        this.currentValue = field_value
    },
    getCommonContext() {
        let label = getOpt(this, "title", "") || getOpt(this, "label", "")
        let ariaLabel = getOpt(this, "ariaLabel", "") || label
        let placeholder = getOpt(this, "placeholder", ariaLabel)
        let description = getOpt(this, "description", "")
        let editable = getOpt(this, "editable", true)
        let required = getOpt(this, "required", false)
        if (getOpt(this, "value", "")) {
            this.currentValue = getOpt(this, "value", "")
        }
        return {
            ariaLabel: ariaLabel,
            label: label,
            title: label, // For retro-compatibility
            description: description,
            placeholder: placeholder,
            field_name: this.getOption("field_name"),
            tagId: this.getTagId(),
            editable: editable,
            required: required,
        }
    },
})
export default BaseFormWidget
