import AppRouter from "marionette.approuter"

const Router = AppRouter.extend({
    appRoutes: {
        index: "index",
        "": "index",
        addentry: "addEntry",
        "editentry/:id": "editEntryMainData",
        "entries/:id": "editEntry",
    },
})
export default Router
