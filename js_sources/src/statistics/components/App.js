import Bb from "backbone"
import Mn from "backbone.marionette"
import Router from "./Router.js"
import { hideLoader } from "../../tools.js"
import RootComponent from "../views/RootComponent.js"
import Controller from "./Controller.js"

const AppClass = Mn.Application.extend({
    channelName: "app",
    radioEvents: {
        navigate: "onNavigate",
        "show:modal": "onShowModal",
        "entry:delete": "onDelete",
        "entry:export": "onEntryExport",
        "entry:duplicate": "onEntryDuplicate",
        "criterion:delete": "onDelete",
    },
    region: "#js-main-area",
    onBeforeStart(app, options) {
        console.log(" - AppClass : Initializing UI components")
        this.rootView = new RootComponent()
        this.controller = new Controller({ rootView: this.rootView })
        this.router = new Router({ controller: this.controller })
    },
    onStart(app, options) {
        this.showView(this.rootView)
        console.log(" - AppClass : Starting the js history")
        hideLoader()
        Bb.history.start()
    },
    onEntryExport(model) {
        this.controller.showEntryExport(model)
    },
    onEntryDuplicate(model) {
        this.controller.showEntryDuplicate(model)
    },
    onNavigate(route_name, parameters) {
        console.log(" - AppClass.onNavigate : %s", route_name)
        let dest_route = route_name
        if (!_.isUndefined(parameters)) {
            dest_route += "/" + parameters
        }
        window.location.hash = dest_route
        Bb.history.loadUrl(dest_route)
    },
    onShowModal(view) {
        this.controller.showModal(view)
    },
    onDelete(childView) {
        this.controller.modelDelete(childView)
    },
})
const App = new AppClass()
export default App
