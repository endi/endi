import Radio from "backbone.radio"
import Mn from "backbone.marionette"
import EntryListComponent from "./EntryListComponent"
import EntryFormComponent from "./entryForm/EntryFormComponent"
import ActionToolbar from "common/views/ActionToolbar.js"
import EntryAddForm from "./EntryAddForm"
import EntryDuplicateForm from "./EntryDuplicateForm"

const RootComponent = Mn.View.extend({
    template: require("./templates/RootComponent.mustache"),
    regions: {
        toolbar: "#toolbar",
        main: "#main",
        popup: ".entry-popup",
    },
    initialize() {
        this.facade = Radio.channel("facade")
        this.config = Radio.channel("config")
        this.app = Radio.channel("app")
    },
    _showSheetToolbar() {
        const actions = this.config.request("get:form_actions")
        actions["main"] = [
            {
                option: {
                    url: "#addentry",
                    label: "Ajouter une entrée",
                    icon: "plus",
                    title: "Ajouter une entrée statistiques",
                    css: "btn btn-primary icon",
                },
                widget: "anchor",
            },
        ]
        const view = new ActionToolbar(actions)
        this.showChildView("toolbar", view)
    },
    _triggerEntryDelete(event) {
        console.log("Trigger entry delete")
        this.app.trigger("entry:delete", this.getChildView("main"))
    },
    _showEntryToolbar(entry) {
        const actions = {
            main: [
                {
                    option: {
                        label: "Revenir à la liste",
                        icon: "arrow-left",
                        title: "Revenir à la liste des entrées",
                        url: "#index",
                        css: "btn icon",
                    },
                    widget: "anchor",
                },
            ],
            more: [
                {
                    option: {
                        icon: "copy",
                        title: "Dupliquer cette entrée statistiques",
                        onclick: (event) =>
                            this.app.trigger("entry:duplicate", entry),
                        css: "btn icon only",
                    },
                    widget: "button",
                },
                {
                    option: {
                        icon: "file-csv",
                        title: "Exporter les données de gestion sociale associée à cette entrée",
                        onclick: (event) =>
                            this.app.trigger("entry:export", entry),
                        css: "btn icon only",
                    },
                    widget: "button",
                },
                {
                    option: {
                        icon: "trash-alt",
                        title: "Supprimer cette entrée statistiques",
                        onclick: (event) => this._triggerEntryDelete(event),
                        css: "btn icon only",
                    },
                    widget: "button",
                },
            ],
        }
        console.log("Show entry toolbar")
        console.log(actions)
        const view = new ActionToolbar(actions)
        this.showChildView("toolbar", view)
    },
    showEntryDuplicateForm(entry) {
        const sheets = this.config.request("get:options", "sheet_list")

        const view = new EntryDuplicateForm({ model: entry, sheets: sheets })
        this.showChildView("popup", view)
    },
    _showEntryAdd(entry, entryCollection) {
        const view = new EntryAddForm({
            model: entry,
            destCollection: entryCollection,
            add: true,
        })
        this.showChildView("main", view)
    },
    _showEntryEditMainData(entry, entryCollection) {
        const view = new EntryAddForm({
            model: entry,
            destCollection: entryCollection,
            add: false,
        })
        this.showChildView("main", view)
    },
    _showEntryEdit(entry, criteriaCollection) {
        const view = new EntryFormComponent({
            model: entry,
            criteriaCollection: criteriaCollection,
        })
        this.showChildView("main", view)
    },
    _showEntryList() {
        const collection = this.facade.request("get:collection", "entries")
        const view = new EntryListComponent({ collection: collection })
        this.showChildView("main", view)
    },
    entryAdd(entry, entryCollection) {
        this._showEntryAdd(entry, entryCollection)
    },
    entryEdit(entry, criteriaCollection) {
        this._showEntryToolbar(entry)
        this._showEntryEdit(entry, criteriaCollection)
    },
    entryEditMainData(entry, entryCollection) {
        this._showEntryEditMainData(entry, entryCollection)
    },
    index() {
        console.log("Index view")
        this._showSheetToolbar()
        this._showEntryList()
    },
})
export default RootComponent
