import Mn from "backbone.marionette"
import Radio from "backbone.radio"
import CombinationView from "./CombinationView"

const template = require("./templates/ClauseCriteriaView.mustache")

const ClauseCriteriaView = Mn.View.extend({
    tagName: "li",
    template: template,
    regions: {
        children: { el: ".children", replaceElement: true },
        combination: ".combination",
    },
    childViewEvents: { "action:clicked": "onAddClick" },
    childViewTriggers: {
        "criterion:delete": "criterion:delete",
        "add:click": "add:click",
    },
    initialize() {
        this.config = Radio.channel("config")
        this.facade = Radio.channel("facade")
    },
    templateContext() {
        // Collect data sent to the template (model attributes are already transmitted)
        return {}
    },
    onAddClick(action_name) {
        /*
         * add click on ouvre une popup pour sélectionner le champ à traiter
         * on met le parent_id en mémoire dans un coin
         */
        if (action_name == "add") {
            console.log("ClauseCriteriaView.onAddClick")
            console.log(this.model)
            this.trigger("add:click", this.model)
        }
    },
    showCombination() {
        // Affichage du toggle ET/OU
        if (
            this.model.collection.indexOf(this.model) >= 1 &&
            this.model.collection.path.length == 0
        ) {
            const view = new CombinationView({
                model: this.model.collection._parent,
            })
            this.showChildView("combination", view)
        }
    },
    onRender() {
        this.showCombination()
        if (this.model.children.length > 0) {
            this.showChildView(
                "children",
                new this.__parentclass__({
                    collection: this.model.children,
                    parent: this,
                })
            )
        }
    },
})
export default ClauseCriteriaView
