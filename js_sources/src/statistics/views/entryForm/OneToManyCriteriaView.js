import Mn from "backbone.marionette"
import Radio from "backbone.radio"
import ButtonModel from "base/models/ButtonModel"
import ButtonWidget from "widgets/ButtonWidget"
import CombinationView from "./CombinationView"

const template = require("./templates/OneToManyCriteriaView.mustache")

const OneToManyCriteriaView = Mn.View.extend({
    tagName: "li",
    template: template,
    regions: {
        children: { el: ".children", replaceElement: true },
        combination: ".combination",
        deleteButton: { el: ".delbutton-container", replaceElement: true },
    },
    childViewEvents: {
        "add:click": "onAddClick",
        "action:clicked": "onActionClicked",
    },
    childViewTriggers: {
        "criterion:delete": "criterion:delete",
    },
    initialize() {
        this.config = Radio.channel("config")
        this.facade = Radio.channel("facade")
        this.statService = Radio.channel("stat")
        this.relationshipConfig = this.statService.request(
            "find:column",
            this.model.get("key"),
            this.model.collection.path
        )
        console.log(this.relationshipConfig)
    },
    onAddClick() {
        /*
         * add click on ouvre une popup pour sélectionner le champ à traiter
         * on met le parent_id en mémoire dans un coin
         */
        console.log("ClauseCriteriaView.onAddClick")
        console.log(this.model)
        this.trigger("add:click", this.model)
    },
    onActionClicked(action_name) {
        /*
         * add click on ouvre une popup pour sélectionner le champ à traiter
         * on met le parent_id en mémoire dans un coin
         */
        console.log("OneToMany.onActionClicked")
        if (action_name == "delete") {
            this.trigger("criterion:delete", this.model)
        } else if (action_name == "add") {
            this.trigger("add:click", this.model)
        }
    },
    templateContext() {
        // Collect data sent to the template (model attributes are already transmitted)
        return { label: this.relationshipConfig["label"] }
    },
    showButtons() {
        /*
         * Show Delete and optionnal Add button
         */
        const model = new ButtonModel({
            label: "Supprimer",
            showLabel: false,
            action: "delete",
            icon: "trash-alt",
        })
        const button = new ButtonWidget({
            model: model,
            surroundingTagName: "div",
            surroundingCss: "actions",
        })
        this.showChildView("deleteButton", button)
    },
    onRender() {
        this.showButtons()
        console.log(this.model)
        if (
            this.model.collection.indexOf(this.model) >= 1 &&
            this.model.collection.path.length == 0
        ) {
            const view = new CombinationView({
                model: this.model.collection._parent,
            })
            this.showChildView("combination", view)
        }
        if (this.model.children.length > 0) {
            this.showChildView(
                "children",
                new this.__parentclass__({
                    collection: this.model.children,
                    parent: this,
                })
            )
        }
    },
})
export default OneToManyCriteriaView
