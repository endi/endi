import Bb from "backbone"

import CommonModel from "./CommonModel"
import OneToManyModel from "./OneToManyModel"
import ClauseModel from "./ClauseModel"

const modelMap = {
    manytoone: CommonModel,
    static_opt: CommonModel,
    or: ClauseModel,
    and: ClauseModel,
    onetomany: OneToManyModel,
    bool: CommonModel,
    string: CommonModel,
    date: CommonModel,
    multidate: CommonModel,
    number: CommonModel,
}

const CriteriaCollection = Bb.Collection.extend({
    initialize: function (models, options) {
        options = options || {}
        if ("path" in options) {
            this.path = options["path"]
        } else {
            this.path = []
        }
        if ("url" in options) {
            this.url = options["url"]
        }
    },
    model(modelData, options) {
        const typ = modelData.type
        if ((!typ) in modelMap) {
            console.error("Unknown model typ %s", typ)
        }
        return new modelMap[typ](modelData, options)
    },
})
CriteriaCollection.prototype.__class__ = CriteriaCollection
export default CriteriaCollection
