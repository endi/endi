import Bb from "backbone"

const SheetModel = Bb.Model.extend({
    validation: {
        title: {
            required: true,
            msg: "est requis",
        },
    },
    exportUrl() {
        return this.url() + ".csv"
    },
})
export default SheetModel
