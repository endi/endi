import BaseModel from "../../base/models/BaseModel"

const EntryModel = BaseModel.extend({
    props: ["id", "title", "description", "sheet_id", "criteria"],
    defaults: { criteria: [] },
    validation: {
        title: {
            required: true,
            msg: "est requis",
        },
    },
    exportUrl() {
        return "/statistics/entries/" + this.get("id") + ".csv"
    },
    criteria_url: function () {
        return this.url() + "/criteria"
    },
    duplicateUrl() {
        return this.url() + "?action=duplicate"
    },
})
export default EntryModel
