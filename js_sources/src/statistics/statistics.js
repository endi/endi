/* global AppOption; */
import $ from "jquery"
import _ from "underscore"

import { applicationStartup } from "../backbone-tools.js"
import App from "./components/App.js"
import StatService from "./components/StatService.js"
import Facade from "./components/Facade.js"

$(function () {
    console.log(" # Page Loaded : starting the js code #")
    applicationStartup(AppOption, App, Facade, {
        customServices: [StatService],
    })
})
