/* global AppOption */
import Bb from "backbone"
import Radio from "backbone.radio"
import { round } from "../../math.js"
import DuplicableMixin from "../../base/models/DuplicableMixin.js"

const BaseLineCollection = Bb.Collection.extend(BaseLineCollection)
    .extend(DuplicableMixin)
    .extend({
        initialize() {
            this.on("remove", this.channelCall)
            this.on("sync", this.channelCall)
            this.on("reset", this.channelCall)
            this.on("add", this.channelCall)
        },
        channelCall: function (model) {
            console.log("Triggering Channel call")
            var channel = Radio.channel("facade")
            channel.trigger("changed:line")
        },
        url() {
            return AppOption["context_url"] + "/lines"
        },
        total_ht: function () {
            var result = 0
            this.each(function (model) {
                result += round(model.getHT())
            })
            return result
        },
        total_tva: function () {
            var result = 0
            this.each(function (model) {
                result += round(model.getTva())
            })
            return result
        },
        total: function () {
            var result = 0
            this.each(function (model) {
                result += round(model.total())
            })
            return result
        },
    })
export default BaseLineCollection
