/*
 * File Name : ActionButtonModel.js
 * Kept for compatibility use ButtonModel instead
 */
import ButtonModel from "./ButtonModel.js"

const ActionButtonModel = ButtonModel.extend({})

export default ActionButtonModel
