import Radio from "backbone.radio"

import BaseModel from "../../base/models/BaseModel.js"
import DuplicableMixin from "../../base/models/DuplicableMixin.js"

const BaseLineModel = BaseModel.extend(DuplicableMixin).extend({
    defaults: {
        description: "",
        ht: null,
        tva: null,
    },
    initialize(options) {
        this.config = Radio.channel("config")
    },
    // Validation rules for our model's attributes
    validation: {
        ht: {
            required: true,
            pattern: "amount",
            msg: "doit être un nombre",
        },
        tva: {
            required: true,
            pattern: "amount",
            msg: "doit être un nombre",
        },
        type_id: {
            required: true,
            msg: "est requis",
        },
    },
    total: function () {
        var total = this.getHT() + this.getTva()
        return total
    },
    getTva: function () {
        var result = parseFloat(this.get("tva"))
        return result
    },
    getHT: function () {
        var result = parseFloat(this.get("ht"))
        return result
    },
    getType() {
        return this.config.request("get:ExpenseType", this.get("type_id"))
    },
})
export default BaseLineModel
