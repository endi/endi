import Mn from "backbone.marionette"
import Validation from "backbone-validation"

const BaseFormBehavior = Mn.Behavior.extend({
    onDataPersist: function (attribute, value) {
        console.log("BaseFormBehavior.onDataPersist")
        Validation.unbind(this.view)
        Validation.bind(this.view, {
            attributes: function (view) {
                return [attribute]
            },
        })

        var datas = {}
        datas[attribute] = value
        this.view.model.set(datas)
        this.view.triggerMethod("data:persisted", datas)
    },
    onDataModified: function (attribute, value) {
        Validation.unbind(this.view)
        Validation.bind(this.view, {
            attributes: function (view) {
                return [attribute]
            },
        })
        var datas = {}
        datas[attribute] = value
        this.view.model.set(datas)
        this.view.model.isValid()
    },
})
export default BaseFormBehavior
