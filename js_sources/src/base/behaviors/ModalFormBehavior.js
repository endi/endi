import Mn from "backbone.marionette"

import ModalBehavior from "./ModalBehavior.js"
import FormBehavior from "./FormBehavior.js"

var ModalFormBehavior = Mn.Behavior.extend({
    /*
        Combine modal and form behavior

        fires the following events on the view

        cancel:form when the modal is closed manually

        modal:close


    */
    behaviors: [ModalBehavior, FormBehavior],
    onSuccessSync() {
        console.log("ModalFormBehavior.onSuccessSync")
        console.log("Trigger modal:close from ModalFormBehavior")
        this.view.triggerMethod("modal:close")
    },
    onModalBeforeClose() {
        console.log("Trigger reset:model from ModalFormBehavior")
        this.view.triggerMethod("reset:model")
        this.view.triggerMethod("cancel:form")
    },
    onCancelForm() {
        console.log("ModalFormBehavior.onCancelForm")
        console.log("Trigger modal:close from ModalFormBehavior")
        this.view.triggerMethod("modal:close")
    },
    onModalClose() {
        console.log("ModalFormBehavior.onModalClose")
    },
})

export default ModalFormBehavior
