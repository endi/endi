import Mn from "backbone.marionette"
import Radio from "backbone.radio"

const UserSessionClass = Mn.Object.extend({
    channelName: "session",
    radioRequests: {
        get: "getEntry",
        set: "setEntry",
    },
    initialize(options) {
        this._datas = {}
    },
    getEntry(key, default_value) {
        return this._datas.hasOwnProperty(key)
            ? this._datas[key]
            : default_value
    },
    setEntry(key, value) {
        console.log("UserSession Storing %s %s", key, value)
        this._datas[key] = value
    },
})
const UserSession = new UserSessionClass()
export default UserSession
