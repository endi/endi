/*
 * Setup the main ui elements
 */
import { attachTools } from "./tools.js"
import "bootstrap/js/transition.js"
import "bootstrap/js/dropdown.js"
import "bootstrap/js/collapse.js"
import "select2/dist/js/select2.js"
import "select2/dist/js/i18n/fr"

function format_company_select_labels(state) {
    /*
        N'affiche que le texte avant le premier '##'
        dans les items du sélecteur d'enseigne
        (ce qui est après ne sert que pour rechercher)
    */
    return state.text.split("##")[0]
}

$(function () {
    attachTools()
    var company_menu = $("#company-select-menu")
    if (
        !_.isNull(company_menu) &&
        !company_menu.hasClass("select2-hidden-accessible")
    ) {
        company_menu.select2({
            templateSelection: format_company_select_labels,
            templateResult: format_company_select_labels,
            language: "fr",
        })
        company_menu.on("change", function () {
            window.location = this.value
        })
    }
    $("a[data-toggle=dropdown]").dropdown()
})
