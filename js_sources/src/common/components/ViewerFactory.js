import Mn from "backbone.marionette"
import ImageViewerView from "../views/ImageViewerView"
import PDFViewerView from "../views/PDFViewerView"

const NodeFileViewerFactoryClass = Mn.object.extend({
    typesMap: {
        "image/jpeg": ImageViewerView,
        "image/svg+xml": ImageViewerView,
        "image/gif": ImageViewerView,
        "image/bitmap": ImageViewerView,
        "image/png": ImageViewerView,
        "image/svg": ImageViewerView,
        "image/webp": ImageViewerView,
        "image/apng": ImageViewerView,
        "image/tiff": ImageViewerView,
        "application/pdf": PDFViewerView,
    },
    getViewer(file, kwargs) {
        console.log("NodeFileViewer.getViewer", file, kwargs)
        klass = this.typesMap[file.mimetye]
        if (klass) {
            let args = Object.assign(
                { fileUrl: `/files/${files[0]}?action=stream` },
                kwargs
            )
            return klass(args)
        } else {
            return null
        }
    },
})

const NodeFileViewerFactory = new NodeFileViewerFactoryClass()

export default NodeFileViewerFactory
