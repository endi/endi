import Mn from "backbone.marionette"

import { getOpt } from "../../tools.js"
import ActionListView from "./ActionListView.js"

var template = require("./templates/ActionToolbar.mustache")

/** An horizontal toolbar with three zones
 *
 * - *main* on left for main action buttons
 *    (typically: validation/save buttons)
 * - *more* on right for secondary action buttons
 *    (typically: duplicate/export buttons)
 * - *resume* at center for any other information/controls
 *    (typically: a summary of document)
 *
 * new ActionToolBar(
 * { 'main': [liste de dict décrivant les actions principales]
 * 'more': [liste de dict décrivant les actions secondaires],
 *  'resume': <Instance de View déjà créée>
 )
*/
const ActionToolbar = Mn.View.extend({
    template: template,
    className: "layout flex main_actions",
    regions: {
        main_actions: {
            el: ".main_actions",
            replaceElement: true,
        },
        more_actions: {
            el: ".more_actions",
            replaceElement: true,
        },
        resume: ".resume",
    },
    childViewTriggers: {
        "status:change": "status:change",
    },
    onRender() {
        console.log("RENDERING THE ACTION TOOLBAR")
        const main_actions = getOpt(this, "main", null)
        const more_actions = getOpt(this, "more", null)
        const resume_view = getOpt(this, "resume", null)
        if (main_actions !== null) {
            this.showChildView(
                "main_actions",
                new ActionListView({
                    actions: main_actions,
                })
            )
        }
        if (more_actions !== null) {
            this.showChildView(
                "more_actions",
                new ActionListView({
                    actions: more_actions,
                })
            )
        }
        if (resume_view != null) {
            this.showChildView("resume", resume_view)
        }
    },
})
export default ActionToolbar
