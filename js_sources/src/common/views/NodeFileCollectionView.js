import _ from "underscore"
import Mn from "backbone.marionette"
import NodeFileItemView from "./NodeFileItemView"
import Radio from "backbone.radio"

const template = require("./templates/NodeFileCollectionView.mustache")

const Emptyview = Mn.View.extend({
    template: _.template(
        "<em>Aucun justificatif n’a été déposé pour l’instant"
    ),
    tagName: "li",
})

const NodeFileCollectionView = Mn.CollectionView.extend({
    template: template,
    childView: NodeFileItemView,
    emptyView: Emptyview,
    childViewContainer: "ul",
    ui: {
        add_btn: ".add-btn",
    },
    events: {
        "click @ui.add_btn": "onAdd",
    },
    initialize: function (options) {
        this.facade = Radio.channel("facade")
    },
    onAdd() {
        this.facade.request("save:all")
        window.openPopup(window.location.pathname + "/addfile")
    },
})
export default NodeFileCollectionView
