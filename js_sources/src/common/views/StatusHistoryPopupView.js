import Mn from "backbone.marionette"
import ModalBehavior from "../../base/behaviors/ModalBehavior"
import StatusLogEntryCollectionView from "./StatusLogEntryCollectionView.js"

/** Popup with status history full list
 *
 * Similar StatusHistoryView but w/o "show more" feature, and within a popup.
 */
const StatusHistoryPopupView = Mn.View.extend({
    behaviors: [ModalBehavior],
    template: require("./templates/StatusHistoryPopupView.mustache"),
    regions: {
        comments: {
            el: ".comments",
            replaceElement: true,
        },
    },
    childViewTriggers: {
        "statuslogentry:edit": "statuslogentry:edit",
    },

    initialize(options) {
        this.collection = options.collection
        this.focusedModel = options.focusedModel
    },
    onRender() {
        this.showChildView(
            "comments",
            new StatusLogEntryCollectionView({
                collection: this.collection,
                focusedModel: this.focusedModel,
            })
        )
    },
    templateContext() {
        return { collectionLength: this.collection.length }
    },
})

export default StatusHistoryPopupView
