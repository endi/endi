import Mn from "backbone.marionette"

const template = require("./templates/DocumentViewerControlsView.mustache")

const DocumentViewerControlsView = Mn.View.extend({
    ui: {
        zoom_in_btn: ".zoom_in",
        zoom_out_btn: ".zoom_out",
        go_previous_btn: ".go_previous",
        go_next_btn: ".go_next",
        open_full_btn: ".open_full",
        rotate_right_btn: ".rotate-right",
        rotate_left_btn: ".rotate-left",
    },
    template: template,

    triggers: {
        "click @ui.zoom_in_btn": "zoom:in",
        "click @ui.zoom_out_btn": "zoom:out",
        "click @ui.go_previous_btn": "page:previous",
        "click @ui.go_next_btn": "page:next",
        "click @ui.open_full_btn": "open",
        "click @ui.rotate_right_btn": "rotate:right",
        "click @ui.rotate_left_btn": "rotate:left",
    },

    templateContext() {
        return {
            multipage: this.getOption("multipage"),
        }
    },
})

export default DocumentViewerControlsView
