import Mn from "backbone.marionette"
import { displayServerError } from "../../backbone-tools"
import Radio from "backbone.radio"

const template = require("./templates/NodeFileItemView.mustache")

const NodeFileItemView = Mn.View.extend({
    ui: {
        edit_btn: ".edit-btn",
        download_btn: ".download-btn",
    },
    events: {
        "click @ui.edit_btn": "onEdit",
        "click @ui.download_btn": "onDownload",
    },
    tagName: "li",
    template: template,

    initialize: function (options) {
        this.facade = Radio.channel("facade")
    },
    onEdit() {
        this.facade.request("save:all")
        window.openPopup("/files/" + this.model.get("id"))
    },
    onDownload() {
        this.facade.request("save:all")
        window.openPopup("/files/" + this.model.get("id") + "?action=download")
    },
})
export default NodeFileItemView
