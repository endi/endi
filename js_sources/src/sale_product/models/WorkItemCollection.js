/*
 * File Name :  WorkItemCollection
 */
import OrderableCollection from "../../base/models/OrderableCollection.js"
import Radio from "backbone.radio"
import WorkItemModel from "./WorkItemModel.js"
import { ajax_call } from "../../tools.js"
import { strToFloat } from "math.js"

const WorkItemCollection = OrderableCollection.extend({
    model: WorkItemModel,
    load_from_catalog: function (sale_products) {
        var serverRequest = ajax_call(
            this.url() + "?action=load_from_catalog",
            {
                sale_products: sale_products,
            },
            "POST"
        )
        return serverRequest.then(this.fetch.bind(this))
    },
    syncAll(models) {
        console.log("WorkItemCollection Syncing all models")
        var promises = []
        this.each(function (model) {
            promises.push(model.fetch())
        })
        var resulting_deferred = $.when(...promises)
        return resulting_deferred
    },
})
export default WorkItemCollection
