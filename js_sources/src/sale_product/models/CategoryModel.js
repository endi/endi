/*
 * File Name :  CategoryModel
 */
import Bb from "backbone"
import BaseModel from "../../base/models/BaseModel.js"

const CategoryModel = BaseModel.extend({
    props: ["id", "title", "description"],
    validation: {
        title: { required: true, msg: "Le titre est requis" },
    },
    default: {
        description: "",
    },
})
export default CategoryModel
