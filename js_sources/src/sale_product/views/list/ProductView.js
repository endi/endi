/*
 * File Name : ProductView.js
 */
import Mn from "backbone.marionette"
import Radio from "backbone.radio"

import ButtonCollection from "../../../base/models/ButtonCollection.js"
import ButtonModel from "../../../base/models/ButtonModel.js"
import ActionButtonsWidget from "../../../widgets/ActionButtonsWidget.js"
import { formatDate } from "../../../date"

const template = require("./templates/ProductView.mustache")

const ProductView = Mn.View.extend({
    tagName: "tr",
    className: "white_tr clickable-row",
    template: template,
    regions: {
        actions: "td.col_actions",
    },
    events: {
        "click @ui.clickableTd": "onLineClicked",
    },
    ui: {
        edit: ".edit",
        delete: ".delete",
        duplicate: ".duplicate",
        clickableTd: "td:not(.col_actions)",
    },
    childViewEvents: {
        "action:clicked": "onActionClicked",
    },
    getViewButtonModel(label, action, icon, showLabel = false, css = "") {
        return { label, action, showLabel, icon, css }
    },
    onRender() {
        const primary = [
            this.getViewButtonModel("Voir / Modifier", "edit", "pen"),
            this.getViewButtonModel("Dupliquer", "duplicate", "copy"),
        ]
        if (!this.model.get("locked")) {
            primary.push(
                this.getViewButtonModel(
                    "Supprimer",
                    "delete",
                    "trash-alt",
                    false,
                    "negative"
                )
            )
        } else {
            primary.push(
                this.getViewButtonModel("Archiver", "archive", "archive", false)
            )
        }
        let view = new ActionButtonsWidget({
            primary,
        })
        this.showChildView("actions", view)
    },
    onLineClicked() {
        this.onActionClicked("edit")
    },
    onActionClicked(actionName) {
        this.triggerMethod("model:" + actionName, this)
    },
    templateContext() {
        let icons = false
        if (this.model.is_complex) {
            icons = this.model.getIcons()
        }

        return {
            id: this.model.get("id"),
            category_label: this.model.category_label(),
            tvaMode: this.getOption("tvaMode"),
            tva_label: this.model.tva_label(),
            ht_label: this.model.ht_label(),
            supplier_ht_label: this.model.supplier_ht_label(),
            supplier_label: this.model.supplier_label(),
            supplier_ref: this.model.get("supplier_ref"),
            current_stock: this.model.get("current_stock"),
            updated_at: formatDate(this.model.get("updated_at")),
            icons: icons,
            tooltipTitle:
                "Cliquer pour voir le produit « " +
                this.model.get("label") +
                " »",
        }
    },
})
export default ProductView
