/*
 * Module name : WorkItemEmptyView
 */
import Mn from "backbone.marionette"
import Radio from "backbone.radio"

const template = require("./templates/WorkItemEmptyView.mustache")

const WorkItemEmptyView = Mn.View.extend({
    template: template,
    tagName: "tr",
})
export default WorkItemEmptyView
