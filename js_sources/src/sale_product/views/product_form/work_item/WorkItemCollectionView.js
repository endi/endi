/*
 * Module name : WorkItemCollectionView
 */
import Mn from "backbone.marionette"

import WorkItemEmptyView from "./WorkItemEmptyView.js"
import WorkItemView from "./WorkItemView.js"

const WorkItemCollectionView = Mn.CollectionView.extend({
    emptyView: WorkItemEmptyView,
    tagName: "tbody",
    childView: WorkItemView,
    childViewTriggers: {
        "model:edit": "model:edit",
        "model:delete": "model:delete",
    },
})
export default WorkItemCollectionView
