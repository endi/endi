/*
Global Api, handling all the model and collection fetch

facade = Radio.channel('facade');
facade.request('get:collection', 'products');
*/
import Mn from "backbone.marionette"
import Bb from "backbone"
import Radio from "backbone.radio"
import { round } from "../../math.js"
import FacadeModelApiMixin from "../../base/components/FacadeModelApiMixin.js"

import CategoryCollection from "../models/CategoryCollection.js"
import ProductCollection from "../models/ProductCollection.js"
import FilterModel from "../models/FilterModel.js"

const FacadeClass = Mn.Object.extend(FacadeModelApiMixin).extend({
    radioRequests: {
        "get:collection": "getCollectionRequest",
        "get:collection:filter": "getCollectionFilterRequest",
        "get:collection:firstpage": "getCollectionFirstPageRequest",
        "get:collection:lastpage": "getCollectionLastPageRequest",
        "get:collection:nextpage": "getCollectionNextPageRequest",
        "get:collection:page": "getCollectionPageRequest",
        "get:collection:previouspage": "getCollectionPreviousPageRequest",
        "get:collection:refresh": "getCollectionRefresh",
        "get:model": "getModelRequest",
        "load:collection": "loadCollection",
        "load:model": "loadModel",
        "set:collection:itemsperpage": "setCollectionItemsPerPage",
    },
    initialize(options) {
        this.models = {}
        this.models["ui_list_filter"] = new FilterModel()

        this.collections = {}
        let collection

        collection = new ProductCollection()
        this.collections["products"] = collection

        collection = new CategoryCollection()
        this.collections["categories"] = collection
    },
    setup(options) {
        this.setCollectionUrl("categories", options["category_url"])
        this.setCollectionUrl("products", options["context_url"])
    },
    start() {
        /*
         * Fires initial One Page application Load
         */
        return this.loadCollection("categories")
    },
    sendAjaxError(result) {
        let messageBus = Radio.channel("message")
        messageBus.trigger("error:ajax", result)
    },
    getCollectionFilterRequest(label, query_params) {
        /*
         * Apply a filter on the given collection
         */
        console.log("Facade.getCollectionFilterRequest")
        console.log(query_params)
        this.collections[label].updateParams(query_params)
        let this_ = this
        return this.collections[label].getFirstPage().fail(this.sendAjaxError)
    },
    getCollectionFirstPageRequest(label) {
        console.log("Facade.getCollectionFirstPageRequest")
        return this.collections[label].getFirstPage().fail(this.sendAjaxError)
    },
    getCollectionLastPageRequest(label) {
        return this.collections[label].getLastPage().fail(this.sendAjaxError)
    },
    getCollectionPageRequest(label, page) {
        return this.collections[label].getPage(page).fail(this.sendAjaxError)
    },
    getCollectionNextPageRequest(label) {
        return this.collections[label].getNextPage().fail(this.sendAjaxError)
    },
    getCollectionPreviousPageRequest(label) {
        return this.collections[label]
            .getPreviousPage()
            .fail(this.sendAjaxError)
    },
    getCollectionRefresh(label) {
        let collection = this.collections[label]
        return collection
            .getPage(collection.currentPage)
            .fail(this.sendAjaxError)
    },
    loadModel(collectionName, modelId) {
        const collection = this.collections[collectionName]
        return collection.fetchSingle(modelId)
    },
    setCollectionItemsPerPage(label, items_per_page) {
        this.collections[label].updatePageSize(items_per_page)
        return this.collections[label].getFirstPage().fail(this.sendAjaxError)
    },
})
const Facade = new FacadeClass()
export default Facade
