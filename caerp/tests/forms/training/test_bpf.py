import colander
import pytest
import datetime

from caerp.models.services.bpf import Cerfa_10443_17


@pytest.fixture
def mk_business_bpf_appstruct(invoice, training_speciality):
    def _mk_business_bpf_appstruct(**kwargs):
        appstruct = {
            "cerfa_version": colander.null,
            "financial_year": str(datetime.date.today().year),
            "has_remote": "false",
            "has_subcontract": "no",
            "has_subcontract_amount": "0",
            "has_subcontract_headcount": "0",
            "has_subcontract_hours": "0",
            "headcount": "1",
            "income_sources": [
                {
                    "id": "1",
                    "income_category_id": "1",
                    "invoice_id": str(invoice.id),
                },
            ],
            "is_subcontract": "false",
            "total_hours": "11.0",
            "trainee_types": [
                {
                    "headcount": "0",
                    "id": "1",
                    "total_hours": "0",
                    "trainee_type_id": "1",
                }
            ],
            "training_goal_id": "1",
            "training_speciality_id": training_speciality.id,
        }
        appstruct.update(**kwargs)
        return appstruct

    return _mk_business_bpf_appstruct


def test_validate_invoice_uniqueness(
    training_business,
    mk_business_bpf_appstruct,
    pyramid_request,
    mk_invoice,
):
    invoice1 = mk_invoice()
    invoice2 = mk_invoice()
    schema = Cerfa_10443_17.get_colander_schema(is_subcontract=False)

    pyramid_request.config = {}
    pyramid_request.matchdict["year"] = str(datetime.date.today().year)
    pyramid_request.context = training_business
    schema = schema.bind(request=pyramid_request)

    # just check no exception
    schema.deserialize(
        mk_business_bpf_appstruct(
            income_sources=[
                {
                    "id": "1",
                    "income_category_id": "1",
                    "invoice_id": str(invoice1.id),
                },
            ]
        )
    )

    # just check no exception
    schema.deserialize(
        mk_business_bpf_appstruct(
            income_sources=[
                {
                    "id": "1",
                    "income_category_id": "1",
                    "invoice_id": str(invoice1.id),
                },
                {
                    "id": "2",
                    "income_category_id": "1",
                    "invoice_id": str(invoice2.id),
                },
            ]
        )
    )

    with pytest.raises(colander.Invalid):
        schema.deserialize(
            mk_business_bpf_appstruct(
                income_sources=[
                    {
                        "id": "1",
                        "income_category_id": "1",
                        "invoice_id": str(invoice1.id),
                    },
                    {
                        "id": "2",
                        "income_category_id": "1",
                        "invoice_id": str(invoice1.id),
                    },
                ]
            )
        )
