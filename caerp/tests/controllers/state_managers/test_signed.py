from caerp.controllers.state_managers import set_signed_status


def test_estimation_set_signed(csrf_request_with_db_and_user, mk_estimation):
    estimation = mk_estimation()
    set_signed_status(csrf_request_with_db_and_user, estimation, "aborted")
    assert estimation.signed_status == "aborted"
    assert estimation.statuses[0].status == "aborted"
    assert estimation.statuses[0].user_id == csrf_request_with_db_and_user.identity.id

    set_signed_status(csrf_request_with_db_and_user, estimation, "signed")
    assert estimation.signed_status == "signed"
    assert estimation.statuses[1].status == "signed"
    assert estimation.statuses[1].user_id == csrf_request_with_db_and_user.identity.id
