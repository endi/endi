import pytest


@pytest.fixture
def payment_line_1(mk_payment_line):
    return mk_payment_line(amount=4000000)


@pytest.fixture
def payment_line_2(mk_payment_line):
    return mk_payment_line(amount=6000000)


@pytest.fixture
def payment_line_3(mk_payment_line):
    return mk_payment_line(amount=50000)


@pytest.fixture
def empty_task(mk_task):
    return mk_task(mode="ht")


@pytest.fixture
def empty_task_ttc(mk_task):
    return mk_task(mode="ttc")


@pytest.fixture
def empty_ht_estimation(mk_estimation):
    return mk_estimation(mode="ht")


@pytest.fixture
def cancelinvoice_1(
    mk_cancelinvoice,
):
    return mk_cancelinvoice(status="valid")


@pytest.fixture
def cancelinvoice_2(
    mk_cancelinvoice,
):
    return mk_cancelinvoice(status="valid")


@pytest.fixture
def payment_one(mk_payment):
    return mk_payment(amount=1500000)


@pytest.fixture
def payment_two(mk_payment):
    return mk_payment(amount=1000000)
