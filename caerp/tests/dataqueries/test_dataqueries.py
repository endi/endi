import datetime
from caerp.dataqueries.base import BaseDataQuery


class ExampleDataQuery(BaseDataQuery):

    name = "example_req"
    label = "Exemple de requête"

    def default_dates(self):
        self.set_dates(
            start=self.date_tools.year_start(),
            end=datetime.date(2023, 6, 30),
        )

    def headers(self):
        return ["col1", "col2", "col3"]

    def data(self):
        data = [
            ["lig1_col1", "lig1_col2", "lig1_col3"],
            ["lig2_col1", "lig2_col2", "lig2_col3"],
            ["lig3_col1", "lig3_col2", "lig3_col3"],
        ]
        return data


def test_dataqueries_registry(config, request_with_config_and_db):
    config.include("caerp.utils.dataqueries")
    from caerp.utils.dataqueries import (
        register_dataquery,
        has_dataquery,
        get_dataqueries,
        get_dataquery,
    )

    register_dataquery(config, ExampleDataQuery)
    assert has_dataquery(request_with_config_and_db, "example_req")

    dq_found = False
    for dq in get_dataqueries(request_with_config_and_db):
        if dq.name == "example_req":
            dq_found = True
    assert dq_found

    example_dq = get_dataquery(request_with_config_and_db, "example_req")
    assert example_dq.label == "Exemple de requête"
    assert example_dq.start_date == datetime.date(datetime.date.today().year, 1, 1)
    assert example_dq.end_date == datetime.date(2023, 6, 30)
