from pyramid.authorization import Authenticated, Everyone
from caerp.utils.security.policy import get_user_principals, global_permits


def test_get_user_principals(pyramid_request, login, mk_group, mk_access_right):
    principals = get_user_principals(pyramid_request, None, login.user)
    assert Authenticated in principals
    assert Everyone in principals
    assert f"user:{login.user.id}" in principals
    assert f"login:{login.id}" in principals

    group = mk_group(name="test_group", label="Test group")
    access_right = mk_access_right(name="test_access_right", groups=[group])
    login._groups.append(group)

    principals = get_user_principals(pyramid_request, None, login.user)
    assert f"group:{group.name}" in principals
    assert f"access_right:{access_right.name}" in principals


def test_global_permits(pyramid_request, user, login, mk_group, mk_access_right):
    from caerp.utils.security.policy import global_permits
    from caerp.consts.access_rights import ACCESS_RIGHTS

    assert global_permits(pyramid_request, None, user, "global.authenticated")
    assert not global_permits(pyramid_request, None, user, "global.test_permission")

    group = mk_group(name="test_group", label="Test group")
    access_right_def = list(ACCESS_RIGHTS.values())[0]
    mk_access_right(name=access_right_def["name"], groups=[group])

    login._groups.append(group)

    assert global_permits(pyramid_request, None, user, "global.authenticated")
    for perm in access_right_def["global_permissions"]:
        assert global_permits(pyramid_request, None, user, perm)
        assert not global_permits(
            pyramid_request, None, user, f"global.other_permission"
        )


def test_get_global_permission():
    from caerp.utils.security.policy import get_global_permission

    assert get_global_permission("company.view") == "global.company_view"
    assert (
        get_global_permission("company.view_salarysheet")
        == "global.company_view_salarysheet"
    )


def test_company_permits(
    pyramid_request,
    user,
    login,
    mk_company,
    mk_user,
    mk_login,
    mk_invoice,
    mk_group,
    mk_access_right,
):
    from caerp.utils.security.policy import company_permits

    company = mk_company(name="Test company")
    company2 = mk_company(name="Test company 2", employee=mk_user())
    user.companies.append(company)

    assert company_permits(pyramid_request, company, user, "company.view")
    assert not company_permits(pyramid_request, company2, user, "company.view")

    assert company_permits(
        pyramid_request, mk_invoice(company=company), user, "company.view"
    )

    other_user = mk_user()
    mk_login(login="admin_user", user=other_user)
    group = mk_group(name="test_group", label="Test group")
    # Groupe ayant les droits pour administrer les enseignes global.company_view
    mk_access_right(name="global_company_supervisor", groups=[group])
    other_user.login._groups.append(group)
    assert company_permits(pyramid_request, company, other_user, "company.view")
    assert not company_permits(
        pyramid_request, company2, other_user, "company.view_salarysheet"
    )
    mk_access_right(name="global_company_access_accounting", groups=[group])

    assert global_permits(
        pyramid_request, None, other_user, "global.company_view_salarysheet"
    )
    assert company_permits(
        pyramid_request, company2, other_user, "company.view_salarysheet"
    )
