from caerp.services.company import find_company_id_from_model


class TestFindCompanyService:
    def test_third_party(self, request_with_config_and_db, customer, supplier, company):
        assert (
            find_company_id_from_model(request_with_config_and_db, customer)
            == company.id
        )
        assert (
            find_company_id_from_model(request_with_config_and_db, supplier)
            == company.id
        )

    def test_task(
        self,
        request_with_config_and_db,
        full_invoice,
        full_estimation,
        mk_payment,
        company,
    ):
        assert (
            find_company_id_from_model(request_with_config_and_db, full_invoice)
            == company.id
        )

        assert (
            find_company_id_from_model(
                request_with_config_and_db, full_invoice.line_groups[0].lines[0]
            )
            == company.id
        )

        assert (
            find_company_id_from_model(
                request_with_config_and_db, full_invoice.discounts[0]
            )
            == company.id
        )

        mk_payment(task_id=full_invoice.id, task=full_invoice)
        assert (
            find_company_id_from_model(
                request_with_config_and_db, full_invoice.payments[0]
            )
            == company.id
        )
        assert (
            find_company_id_from_model(request_with_config_and_db, full_estimation)
            == company.id
        )
        assert (
            find_company_id_from_model(
                request_with_config_and_db, full_estimation.line_groups[0]
            )
            == company.id
        )
        assert (
            find_company_id_from_model(
                request_with_config_and_db, full_estimation.post_ttc_lines[0]
            )
            == company.id
        )
        assert (
            find_company_id_from_model(
                request_with_config_and_db, full_estimation.payment_lines[0]
            )
            == company.id
        )

    def test_project(
        self, request_with_config_and_db, mk_project, mk_phase, mk_business, company
    ):
        project = mk_project(company=company)
        phase = mk_phase(project=project)
        business = mk_business(project=project)
        assert (
            find_company_id_from_model(request_with_config_and_db, project)
            == company.id
        )
        assert (
            find_company_id_from_model(request_with_config_and_db, phase) == company.id
        )
        assert (
            find_company_id_from_model(request_with_config_and_db, business)
            == company.id
        )

    def test_expense(
        self,
        request_with_config_and_db,
        mk_expense_sheet,
        mk_expense_line,
        mk_expense_kmline,
        company,
    ):
        expense_sheet = mk_expense_sheet(company=company)
        expense_line = mk_expense_line(sheet=expense_sheet)
        expense_kmline = mk_expense_kmline(sheet=expense_sheet)
        assert (
            find_company_id_from_model(request_with_config_and_db, expense_sheet)
            == company.id
        )
        assert (
            find_company_id_from_model(request_with_config_and_db, expense_line)
            == company.id
        )
        assert (
            find_company_id_from_model(request_with_config_and_db, expense_kmline)
            == company.id
        )

    def test_price_study(
        self,
        request_with_config_and_db,
        mk_estimation,
        mk_price_study,
        mk_price_study_chapter,
        mk_price_study_discount,
        mk_price_study_product,
        mk_price_study_work,
        mk_price_study_work_item,
        company,
    ):
        estimation = mk_estimation()
        price_study = mk_price_study(task=estimation)
        price_study_chapter = mk_price_study_chapter(price_study=price_study)
        price_study_discount = mk_price_study_discount(price_study=price_study)
        price_study_product = mk_price_study_product(chapter=price_study_chapter)
        price_study_work = mk_price_study_work(chapter=price_study_chapter)
        price_study_work_item = mk_price_study_work_item(
            price_study_work=price_study_work
        )
        assert (
            find_company_id_from_model(request_with_config_and_db, price_study)
            == company.id
        )
        assert (
            find_company_id_from_model(request_with_config_and_db, price_study_chapter)
            == company.id
        )
        assert (
            find_company_id_from_model(request_with_config_and_db, price_study_discount)
            == company.id
        )
        assert (
            find_company_id_from_model(request_with_config_and_db, price_study_product)
            == company.id
        )
        assert (
            find_company_id_from_model(request_with_config_and_db, price_study_work)
            == company.id
        )
        assert (
            find_company_id_from_model(
                request_with_config_and_db, price_study_work_item
            )
            == company.id
        )

    def test_sale_product(
        self,
        request_with_config_and_db,
        mk_sale_product,
        mk_sale_product_work,
        mk_sale_product_work_item,
        company,
    ):
        # Tests le find_company_id_from_model sur le catalogue produit
        sale_product = mk_sale_product(company_id=company.id, company=company)
        sale_product_work = mk_sale_product_work(company_id=company.id, company=company)
        sale_product_work_item = mk_sale_product_work_item(
            sale_product_work=sale_product_work
        )
        assert (
            find_company_id_from_model(request_with_config_and_db, sale_product)
            == company.id
        )
        assert (
            find_company_id_from_model(request_with_config_and_db, sale_product_work)
            == company.id
        )
        assert (
            find_company_id_from_model(
                request_with_config_and_db, sale_product_work_item
            )
            == company.id
        )

    def test_supplier_documents(
        self,
        request_with_config_and_db,
        mk_supplier_invoice,
        mk_supplier_order,
        mk_supplier_payment,
        mk_user_payment,
        mk_supplier_invoice_line,
        mk_supplier_order_line,
        company,
    ):
        supplier_invoice = mk_supplier_invoice(company=company)
        supplier_order = mk_supplier_order(company=company)
        supplier_payment = mk_supplier_payment(supplier_invoice=supplier_invoice)
        user_payment = mk_user_payment(supplier_invoice=supplier_invoice)
        supplier_invoice_line = mk_supplier_invoice_line(
            supplier_invoice=supplier_invoice
        )
        supplier_order_line = mk_supplier_order_line(supplier_order=supplier_order)
        assert (
            find_company_id_from_model(request_with_config_and_db, supplier_invoice)
            == company.id
        )
        assert (
            find_company_id_from_model(request_with_config_and_db, supplier_order)
            == company.id
        )
        assert (
            find_company_id_from_model(request_with_config_and_db, supplier_payment)
            == company.id
        )
        assert (
            find_company_id_from_model(request_with_config_and_db, user_payment)
            == company.id
        )
        assert (
            find_company_id_from_model(
                request_with_config_and_db, supplier_invoice_line
            )
            == company.id
        )
        assert (
            find_company_id_from_model(request_with_config_and_db, supplier_order_line)
            == company.id
        )

    def test_progress_invoicing(
        self,
        request_with_config_and_db,
        mk_invoice,
        mk_business,
        mk_progress_invoicing_plan,
        mk_progress_invoicing_chapter,
        mk_progress_invoicing_product,
        mk_progress_invoicing_work,
        mk_progress_invoicing_work_item,
        company,
    ):
        business = mk_business()
        invoice = mk_invoice(business=business)
        progress_invoicing_plan = mk_progress_invoicing_plan(
            task=invoice, business=business
        )
        progress_invoicing_chapter = mk_progress_invoicing_chapter(
            plan=progress_invoicing_plan
        )
        progress_invoicing_work = mk_progress_invoicing_work(
            chapter=progress_invoicing_chapter
        )
        progress_invoicing_work_item = mk_progress_invoicing_work_item(
            work=progress_invoicing_work
        )
        progress_invoicing_product = mk_progress_invoicing_product(
            chapter=progress_invoicing_chapter
        )

        assert (
            find_company_id_from_model(
                request_with_config_and_db, progress_invoicing_plan
            )
            == company.id
        )
        assert (
            find_company_id_from_model(
                request_with_config_and_db, progress_invoicing_chapter
            )
            == company.id
        )
        assert (
            find_company_id_from_model(
                request_with_config_and_db, progress_invoicing_work
            )
            == company.id
        )
        assert (
            find_company_id_from_model(
                request_with_config_and_db, progress_invoicing_work_item
            )
            == company.id
        )
        assert (
            find_company_id_from_model(
                request_with_config_and_db, progress_invoicing_product
            )
            == company.id
        )

    def test_statuslog_entry(
        self, request_with_config_and_db, invoice, mk_status_log_entry, company
    ):
        statuslog_entry = mk_status_log_entry(node=company)
        assert (
            find_company_id_from_model(request_with_config_and_db, statuslog_entry)
            == company.id
        )
        statuslog_entry = mk_status_log_entry(node=invoice)
        assert (
            find_company_id_from_model(request_with_config_and_db, statuslog_entry)
            == company.id
        )

    def test_business_indicators(
        self,
        request_with_config_and_db,
        mk_project,
        mk_business,
        mk_invoice,
        mk_sale_file_requirement,
        mk_custom_business_indicator,
        company,
    ):
        project = mk_project(company=company)
        business = mk_business(project=project)
        invoice = mk_invoice(business=business, project=project)
        custom_business_indicator = mk_custom_business_indicator(business=business)
        assert (
            find_company_id_from_model(
                request_with_config_and_db, custom_business_indicator
            )
            == company.id
        )

        sale_file_requirement = mk_sale_file_requirement(node=business)
        assert (
            find_company_id_from_model(
                request_with_config_and_db, sale_file_requirement
            )
            == company.id
        )
        sale_file_requirement = mk_sale_file_requirement(node=invoice)
        assert (
            find_company_id_from_model(
                request_with_config_and_db, sale_file_requirement
            )
            == company.id
        )
        sale_file_requirement = mk_sale_file_requirement(node=project)
        assert (
            find_company_id_from_model(
                request_with_config_and_db, sale_file_requirement
            )
            == company.id
        )
