def test_line_from_task(
    mk_task_line,
    mk_internalestimation,
    mk_tva,
    mk_expense_type,
    task_line_group,
    dbsession,
):
    line = mk_task_line(
        tva=0,
        cost=15021000,
        quantity=2,
        group=task_line_group,
    )
    estimation = mk_internalestimation(
        description="Description",
    )
    task_line_group.lines = [line]
    estimation.line_groups = [task_line_group]
    dbsession.merge(estimation)
    dbsession.flush()

    from caerp.models.supply.supplier_order import SupplierOrderLine

    oline = SupplierOrderLine.from_task(estimation)
    assert oline.ht == 30042
    assert oline.tva == 0
    assert oline.description == "Description"
    assert oline.type_id is None

    typ = mk_expense_type(label="type interne", internal=True)
    oline = SupplierOrderLine.from_task(estimation)
    assert oline.type_id == typ.id
