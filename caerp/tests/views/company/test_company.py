from pyramid import testing
from caerp.tests.tools import DummyForm
from caerp.models.company import Company

DATAS = {
    "name": "Compané $& test",
    "goal": "Be the best",
    "contribution": "80",
    "submit": "submit",
    "logo": {},
    "header": {},
}


def test_company_dashboard(config, content, get_csrf_request_with_db, user, company):
    from caerp.views.company.views import company_dashboard
    from caerp.views.third_party.customer.routes import COMPANY_CUSTOMERS_ADD_ROUTE
    from caerp.views.supply.invoices.routes import COMPANY_COLLECTION_ROUTE

    config.add_route("user_expenses", "user_expenses")
    config.add_route("/companies/{id}", "/companies/{cid}")
    config.add_route(COMPANY_CUSTOMERS_ADD_ROUTE, COMPANY_CUSTOMERS_ADD_ROUTE)
    config.add_route(COMPANY_COLLECTION_ROUTE, COMPANY_COLLECTION_ROUTE)

    request = get_csrf_request_with_db()
    request.context = company
    config.add_static_view("static", "caerp:static")
    config.set_security_policy(testing.DummySecurityPolicy(identity=user))
    response = company_dashboard(request)

    assert user.companies[0].name == response["company"].name


def test_company_disable_user_one_company(
    config, login, company, get_csrf_request_with_db
):
    from caerp.views.company.views import CompanyDisableView

    config.add_route("/users/{id}/login", "/users/{id}/login")
    request = get_csrf_request_with_db(context=company)
    request.referrer = "company"
    view = CompanyDisableView(request)
    view()
    assert not company.active
    assert not login.active


def test_company_disable_user_more_companies(
    config, login, company, company2, get_csrf_request_with_db
):
    from caerp.views.company.views import CompanyDisableView

    config.add_route("/users/{id}/login", "/users/{id}/login")
    request = get_csrf_request_with_db(context=company)
    request.referrer = "company"
    view = CompanyDisableView(request)
    view()
    assert not company.active
    # User also belongs to Other companies
    assert login.active
