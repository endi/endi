import pytest

from caerp.views.supply.base_rest_api import (
    BaseRestSupplierDocumentView,
)


class TestBaseRestSupplierDocumentView:
    @pytest.fixture
    def view(self, get_csrf_request_with_db):
        request = get_csrf_request_with_db()
        return BaseRestSupplierDocumentView(request)

    def test__get_expense_types_options(
        self,
        view,
        mk_expense_type,
        mk_supplier_invoice,
        mk_supplier_invoice_line,
    ):
        t1 = mk_expense_type(internal=True)
        t2 = mk_expense_type(internal=False, category="1")
        t3 = mk_expense_type(internal=False, category="2")
        t4 = mk_expense_type(internal=False, category="1")
        t5 = mk_expense_type(internal=False, category=None)
        t6 = mk_expense_type(internal=True, category="2")

        invoice = mk_supplier_invoice()
        invoice.lines = [
            mk_supplier_invoice_line(supplier_invoice_id=invoice.id, expense_type=t4)
        ]
        view.context = invoice

        options = view._get_purchase_types_options()
        assert options == [t3, t4, t5]

        view.context = mk_supplier_invoice(internal=True)
        options = view._get_purchase_types_options()

        assert options == [t1, t6]
