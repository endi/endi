import datetime
import pytest


@pytest.fixture
def remittance(bank, mk_bank_remittance):
    return mk_bank_remittance(
        id="REM_ID",
        payment_mode="chèque",
        remittance_date=datetime.date.today(),
        closed=1,
        bank=bank,
        bank_id=bank.id,
    )


@pytest.fixture
def other_remittance(bank, mk_bank_remittance):
    return mk_bank_remittance(
        id="OTHER_REM_ID",
        payment_mode="chèque",
        remittance_date=datetime.datetime.now(),
        closed=1,
        bank=bank,
        bank_id=bank.id,
    )


@pytest.fixture
def payment(mk_payment, tva, remittance):
    return mk_payment(
        amount=10000000,
        mode="chèque",
        tva=tva,
        bank_remittance=remittance,
    )


@pytest.fixture
def payment_wo_remittance(mk_payment, tva, remittance):
    return mk_payment(
        amount=10000000,
        mode="chèque",
        tva=tva,
        bank_remittance=None,
    )


@pytest.fixture
def payment_same_remittance(mk_payment, tva, remittance):
    return mk_payment(
        amount=20000000,
        mode="chèque",
        tva=tva,
        bank_remittance=remittance,
    )


@pytest.fixture
def payment_other_remittance(mk_payment, tva, other_remittance):
    return mk_payment(
        amount=50000000,
        mode="chèque",
        tva=tva,
        bank_remittance=other_remittance,
    )


@pytest.fixture
def payments(
    payment,
    internalpayment,
    payment_same_remittance,
    payment_other_remittance,
):
    return [
        payment,
        payment_same_remittance,
        internalpayment,
        payment_other_remittance,
    ]


@pytest.fixture
def reproductibility_hack(settings, dbsession):
    """
    Dunno exactly why but this has a side effect on how many lines ar exported

    Without this hack the export results differ wether we run the tests of this
    file alone or with the whole test suite.
    """
    # copy-pasted from wsgi_app
    from caerp import base_configure, prepare_config

    config = prepare_config(**settings)
    return base_configure(config, dbsession, from_tests=True, **settings)


def test_payment_export_page_grouping_disabled(
    reproductibility_hack,
    app_config,
    payments,
    get_csrf_request_with_db,
    mk_config,
):
    """
    Test complex case with internal and non internal entries
    """
    from caerp.views.export.payment import PaymentExportPage

    mk_config("receipts_grouping_strategy", "")
    view = PaymentExportPage(get_csrf_request_with_db())
    entries = view._collect_export_data(payments)
    assert len(entries) == 28


def test_payment_export_page_grouping_enabled(
    reproductibility_hack,
    app_config,
    payments,
    get_csrf_request_with_db,
    mk_config,
):
    from caerp.views.export.payment import PaymentExportPage

    mk_config("receipts_grouping_strategy", "remittance_id")
    view = PaymentExportPage(get_csrf_request_with_db())
    entries = view._collect_export_data(payments)

    # The two entries with the same remittance got groupped,
    # ->  one entry less (which is duplicated in its analytical form, so two lines less).
    assert len(entries) == 26


def test_payment_export_page_grouping_enabled_wo_remittance(
    reproductibility_hack,
    app_config,
    payment_wo_remittance,
    get_csrf_request_with_db,
    mk_config,
):
    # Ref #2997
    from caerp.views.export.payment import PaymentExportPage

    mk_config("receipts_grouping_strategy", "remittance_id")

    view = PaymentExportPage(get_csrf_request_with_db())
    entries = view._collect_export_data([payment_wo_remittance])

    assert len(entries) == 8
